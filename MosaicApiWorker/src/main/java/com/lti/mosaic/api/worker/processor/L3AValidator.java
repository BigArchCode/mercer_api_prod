package com.lti.mosaic.api.worker.processor;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JsonNode;

import in.lnt.constants.Constants;
import in.lnt.exceptions.CustomStatusException;
import in.lnt.validations.FormValidator;

/**
 * 
 * @author rushi
 *
 */
public class L3AValidator extends BaseValidator {

	private static final Logger logger = LoggerFactory.getLogger(L3AValidator.class);

	public L3AValidator(Map<String, String> map) {
		super(map);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.lti.mosaic.api.worker.processor.BaseValidator#processJsonNode(com.
	 * fasterxml.jackson. databind.JsonNode)
	 */
	@Override
	public JsonNode process(Object inputJsonNode) throws CustomStatusException{
		logger.info(">> L3AValidator {}", "{}");
		FormValidator formValidator = new FormValidator();
		JsonNode performValidateAggregateRequest = null;
		try {
			performValidateAggregateRequest = formValidator.performValidateAggregateRequest((JsonNode) inputJsonNode,
					true, false);
		} catch (Exception e) {
			StringWriter stringWriter = new StringWriter();
			e.printStackTrace(new PrintWriter(stringWriter));
			String stackTrace = stringWriter.toString();
			logger.error("Exception occured in perform_L3a Validations stak trace is: "
			 + stackTrace +" message:" + e.getMessage()+ " cause:" +e.getCause());
			throw new CustomStatusException(Constants.HTTPSTATUS_400, e.getMessage());
		}
		return performValidateAggregateRequest;
	}

}
