package com.lti.mosaic.api.worker.processor;

import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.lti.mosaic.api.worker.callback.CallBackManager;
import com.lti.mosaic.api.worker.callback.RequestConstants;

import in.lnt.exceptions.CustomStatusException;
import in.lnt.utility.constants.LoggerConstants;
import in.lnt.utility.general.JsonUtils;
import in.lnt.validations.FormValidator;
import in.lti.mosaic.api.base.bso.StatusBSO;
import in.lti.mosaic.api.base.constants.Constants;
import in.lti.mosaic.api.base.constants.Constants.ValidationTypes;
import in.lti.mosaic.api.base.serializer.ObjectSerializationHandler;
import in.lti.mosaic.api.base.services.DatastoreServices;
import in.lti.mosaic.api.base.services.EventAPIService;

/**
 * @author rushikesh
 *
 */
public abstract class BaseValidator implements Runnable {

	private static final Logger logger = LoggerFactory.getLogger(L1L2Validator.class);

	private Map<String, String> inputMap;
	private String documentId = null;
	private Integer status = null;
	private String validationType;
	private static String loggerMsgStr = "Error while performing  {} {}";
	private static String loggerReqIdStr = "Info: RequestId is";

	public Map<String, String> getInputMap() {
		return inputMap;
	}

	public void setInputMap(Map<String, String> inputMap) {
		this.inputMap = inputMap;
	}

	public BaseValidator(Map<String, String> map) {
		inputMap = map;
		validationType = map.get(Constants.Request.VALIDATIONTYPE);
	}

	public abstract JsonNode process(Object inputJsonNode) throws CustomStatusException;

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {

		logger.info(LoggerConstants.LOG_MAXIQAPI, validationType);

		long documentDownloadedTime = 0;
		long processingTime = 0;
		long documentCreatedTime = 0;
		long startAsyncTime = System.currentTimeMillis();

		String requestId = inputMap.get(Constants.Request.REQUESTID);
		String documentIdFromRequest = inputMap.get(Constants.Request.DOCUMENTID);
		String envName = inputMap.get(Constants.Request.ENVIRONMENTNAME);
		
		String callBackJson = null;
		String callBackUrl = null;
		String callBackApikey = null;
		String document = null;
		
		boolean isCallbacked = false;
		boolean isDownloadFailed = false;
		boolean isUploadFailed = false;
		boolean processFail = false;
		try {
			callBackUrl = CallBackManager.getCallBackUrlFromEnvironmentName(envName);
			callBackApikey = CallBackManager.getCallBackApiKeyFromEnvironmentName(envName);
			
			// fetch document from DS using documentId
			try {
				document = DatastoreServices.download(documentIdFromRequest);

				// Dumping request into MongoDB
				FormValidator.dumpToMongo(inputMap.get(Constants.Request.REQUESTID), document, in.lnt.constants.Constants.REQUEST);

				documentDownloadedTime = System.currentTimeMillis();
				StatusBSO.insertStatusForRequestId(requestId, validationType,
						Constants.RequestStatus.INPUTDOCUMENTDOWNLOADED, status, envName, documentIdFromRequest, null);
				
			}catch(Exception e) {
				logger.error(loggerMsgStr, validationType, e.getMessage());
				isDownloadFailed = true;

				// Mail : Async Workflow changes
				StatusBSO.insertStatusForRequestId(requestId, validationType,
						Constants.RequestStatus.INPUTDOCUMENTDOWNLOADEDERROR, status, envName, documentIdFromRequest, e.getMessage());
				callBackJson = CallBackManager.createCallBackJson(requestId, documentIdFromRequest, 
						Constants.ResponseStatus.HTTPSTATUS_500, Constants.Request.MOSAIC_DOC_DOWNLOAD_FAILED);				
				isCallbacked = true;
				sendResponseAndInsertStatusForReqId(requestId, envName, callBackJson, callBackUrl,
						callBackApikey);
				throw e;
			}
				


			JsonNode finalOutPut = null;

			try {
				JsonNode outPutOfValidations;

				outPutOfValidations = getOutputOfValidations(document);
				
				finalOutPut = outPutOfValidations;
				status = 200;

				processingTime = System.currentTimeMillis();
				StatusBSO.insertStatusForRequestId(requestId, validationType, Constants.RequestStatus.PROCESSINGFINISHED,
						status, envName, documentIdFromRequest, null);
				
			} catch (CustomStatusException cse) {
				logger.error(loggerMsgStr, validationType, cse.getMessage());
				ObjectNode errNode = new ObjectMapper().createObjectNode();
				errNode = JsonUtils.updateErrorNodeForGenericException(errNode, cse);
				finalOutPut = errNode;
				status = cse.getHttpStatus();
				
				processFail = true;
				isCallbacked = processingWhenProcessFails(requestId, documentIdFromRequest, envName, callBackUrl,
						callBackApikey, isCallbacked, cse);
				
			} catch (Exception e) {
				logger.error(loggerMsgStr, validationType, e.getMessage());
				ObjectNode errNode = new ObjectMapper().createObjectNode();
				errNode = JsonUtils.updateErrorNodeForGenericException(errNode, e);
				finalOutPut = errNode;
				status = 500;
				processFail = true;
				isCallbacked = processingWhenProcessFails(requestId, documentIdFromRequest, envName, callBackUrl,
						callBackApikey, isCallbacked, e);
				
			}

			// Dumping request into MongoDB
			FormValidator.dumpToMongo(inputMap.get(Constants.Request.REQUESTID),
					ObjectSerializationHandler.toString(finalOutPut), in.lnt.constants.Constants.RESPONSE);
			
			addEventAsProcessingFinished(requestId);

			// Put the output in document Store
			try {
				documentId = DatastoreServices.writeDocs(ObjectSerializationHandler.toString(finalOutPut),
					requestId + envName);

				documentCreatedTime = System.currentTimeMillis();
				StatusBSO.insertStatusForRequestId(requestId, validationType, Constants.RequestStatus.OUTPUTDOCUMENTCREATED,
					status, envName, documentId, null);
			}catch(Exception e) {
				logger.error(loggerMsgStr, validationType, e.getMessage());
				isUploadFailed = true;
				isCallbacked = callBackJsonAndInsertStatusForReqId(requestId, envName, callBackUrl, callBackApikey,
						isCallbacked, e);
				throw e;
			}


			// send response via CallBackManager Class
			sendResponseViaCallBackManager(requestId, envName, callBackUrl, callBackApikey,
					isCallbacked);

		} catch (Exception e) {
			logger.error(loggerMsgStr, validationType, e.getMessage());

			// Mail : Async Workflow changes
			if(!isCallbacked) {
				processingWhenCallBackIsFalse(requestId, documentIdFromRequest, envName, callBackUrl, callBackApikey,
						e);
			}
			
		}

		if(isDownloadFailed) {
			logger.error(loggerReqIdStr, "{} Document Download : Failed");
		} else {
			logErrorWhenDownloadFails(documentDownloadedTime, processingTime, documentCreatedTime, startAsyncTime,
					requestId, isUploadFailed, processFail);
		}		
	}

	/**
	 * @param documentDownloadedTime
	 * @param processingTime
	 * @param documentCreatedTime
	 * @param startAsyncTime
	 * @param requestId
	 * @param isUploadFailed
	 * @param processFail
	 */
	private void logErrorWhenDownloadFails(long documentDownloadedTime, long processingTime, long documentCreatedTime,
			long startAsyncTime, String requestId, boolean isUploadFailed, boolean processFail) {
		logger.error("Info: RequestId is {} Document Downloaded Time (Millis): {}  ", requestId,
				(documentDownloadedTime - startAsyncTime));
		if(processFail) {
			logger.error(loggerReqIdStr, "{} Document Process : Failed {}");				
		} else {
			logger.error("Info: RequestId is {} API Async Processing Time (Millis): {}  ", requestId,
					(processingTime - documentDownloadedTime));
			if(isUploadFailed) {
				logger.error(loggerReqIdStr, "{} Document Push : Failed");
			} else {
				logger.error("Info: RequestId is {} Document Pushed Time (Millis): {}  ", requestId,
					(documentCreatedTime - processingTime));
			}
		}
	}

	/**
	 * @param requestId
	 * @param documentIdFromRequest
	 * @param envName
	 * @param callBackUrl
	 * @param callBackApikey
	 * @param e
	 */
	private void processingWhenCallBackIsFalse(String requestId, String documentIdFromRequest, String envName,
			String callBackUrl, String callBackApikey, Exception e) {
		String callBackJson;
		if (null == documentId) {
			documentId = inputMap.get(Constants.Request.DOCUMENTID);
		}
		try {
			callBackJson = CallBackManager.createCallBackJson(
					requestId, documentIdFromRequest, Constants.ResponseStatus.HTTPSTATUS_500, e.getMessage());				
			CallBackManager.sendResponseViaCallBackUrl(
					callBackJson, callBackUrl, RequestConstants.POST, callBackApikey);
			StatusBSO.insertStatusForRequestId(requestId, validationType, Constants.RequestStatus.DELIVEREDSUCCESSFULLY, status,
					envName, documentId, null);
		} catch (Exception e1) {
			logger.error(loggerMsgStr, validationType, e1.getMessage());
			StatusBSO.insertStatusForRequestId(requestId, validationType, Constants.RequestStatus.DEADLETTER, status,
					envName, documentId, e1.getMessage());
		}
	}

	/**
	 * @param document
	 * @return
	 * @throws CustomStatusException
	 */
	private JsonNode getOutputOfValidations(String document) throws CustomStatusException {
		JsonNode outPutOfValidations;
		if (StringUtils.equalsIgnoreCase(validationType, ValidationTypes.L3B)) {
			outPutOfValidations = process(document);

		} else {
			JsonNode entitiesNode = (JsonNode) ObjectSerializationHandler.toObject(document, JsonNode.class);
			outPutOfValidations = process(entitiesNode);
		}
		return outPutOfValidations;
	}

	/**
	 * @param requestId
	 * @param envName
	 * @param callBackUrl
	 * @param callBackApikey
	 * @param isCallbacked
	 * @return
	 */
	private boolean sendResponseViaCallBackManager(String requestId, String envName, String callBackUrl,
			String callBackApikey, boolean isCallbacked) {
		String callBackJson;
		try {
			callBackJson = CallBackManager.createCallBackJson(
					requestId, documentId, status, Constants.Request.MOSAIC_PROCESSING_FINISHED);
			isCallbacked = true;
			CallBackManager.sendResponseViaCallBackUrl(callBackJson, callBackUrl, RequestConstants.POST,
					callBackApikey);

			StatusBSO.insertStatusForRequestId(requestId, validationType,
					Constants.RequestStatus.DELIVEREDSUCCESSFULLY, status, envName, documentId, null);

			// Adding event As processing has been delivered successfully
			addEventForSuccessfullProcessingDelivery(requestId);

		} catch (Exception e) {

			logger.error(loggerMsgStr, validationType, e.getMessage());

			StatusBSO.insertStatusForRequestId(requestId, validationType, Constants.RequestStatus.DEADLETTER,
					status, envName, documentId, e.getMessage());

			// Adding event As processing for deadletter
			addProcessingEventForDeadLetter(requestId);
		}
		return isCallbacked;
	}

	/**
	 * @param requestId
	 */
	private void addEventForSuccessfullProcessingDelivery(String requestId) {
		try {
			EventAPIService.addEvent(requestId, Constants.RequestStatus.DELIVEREDSUCCESSFULLY);
		} catch (Exception e) {
			logger.error("Error while adding event {}", e.getMessage());
		}
	}

	/**
	 * @param requestId
	 */
	private void addProcessingEventForDeadLetter(String requestId) {
		try {

			EventAPIService.addEvent(requestId, Constants.RequestStatus.DEADLETTER);
		} catch (Exception exc) {
			logger.error(loggerMsgStr, validationType, exc.getMessage());
		}
	}

	/**
	 * @param requestId
	 * @param envName
	 * @param callBackUrl
	 * @param callBackApikey
	 * @param isCallbacked
	 * @param e
	 * @return
	 * @throws CustomStatusException 
	 * @throws Exception
	 */
	private boolean callBackJsonAndInsertStatusForReqId(String requestId, String envName, String callBackUrl,
			String callBackApikey, boolean isCallbacked, Exception e) throws CustomStatusException {
		String callBackJson;
		boolean isCallback = isCallbacked;
		try {
			
			StatusBSO.insertStatusForRequestId(requestId, validationType, Constants.RequestStatus.OUTPUTDOCUMENTCREATEDERROR,
					status, envName, documentId, e.getMessage());
			
			callBackJson = CallBackManager.createCallBackJson(requestId, "", 
					Constants.ResponseStatus.HTTPSTATUS_500, Constants.Request.MOSAIC_DOC_UPLOAD_FAILED);				
			isCallback = true;
			CallBackManager.sendResponseViaCallBackUrl(
					callBackJson, callBackUrl, RequestConstants.POST, callBackApikey);
			// Mail : Async Workflow changes
			StatusBSO.insertStatusForRequestId(requestId, validationType, Constants.RequestStatus.DELIVEREDSUCCESSFULLY, status,
					envName, documentId, null);
		}catch(Exception se) {
			logger.error(loggerMsgStr, validationType, se.getMessage());
			StatusBSO.insertStatusForRequestId(requestId, validationType, Constants.RequestStatus.DEADLETTER, status,
					envName, documentId, se.getMessage());
			throw new CustomStatusException(Constants.ResponseStatus.HTTPSTATUS_400, se.getMessage());
		}
		return isCallback;
	}

	/**
	 * @param requestId
	 */
	private void addEventAsProcessingFinished(String requestId) {
		try {
			// Adding event As processing has been finished
			EventAPIService.addEvent(requestId, Constants.RequestStatus.PROCESSINGFINISHED);
		} catch (Exception e) {
			logger.error("Error while adding event {}", e.getMessage());
		}
	}

	/**
	 * @param requestId
	 * @param documentIdFromRequest
	 * @param envName
	 * @param callBackUrl
	 * @param callBackApikey
	 * @param isCallbacked
	 * @param cse
	 * @return
	 * @throws CustomStatusException 
	 * @throws Exception
	 */
	private boolean processingWhenProcessFails(String requestId, String documentIdFromRequest, String envName,
			String callBackUrl, String callBackApikey, boolean isCallbacked, Exception cse) throws CustomStatusException
			 {
		String callBackJson;
		boolean isCallback = isCallbacked;
		try {
			
			StatusBSO.insertStatusForRequestId(requestId, validationType, Constants.RequestStatus.PROCESSINGERROR,
					status, envName, documentIdFromRequest, cse.getMessage());
			
			callBackJson = CallBackManager.createCallBackJson(requestId, documentIdFromRequest, 
					Constants.ResponseStatus.HTTPSTATUS_500, Constants.Request.MOSAIC_PROCESSING_FAILED);				
			isCallback = true;
			CallBackManager.sendResponseViaCallBackUrl(
					callBackJson, callBackUrl, RequestConstants.POST, callBackApikey);
			StatusBSO.insertStatusForRequestId(requestId, validationType, Constants.RequestStatus.DELIVEREDSUCCESSFULLY, status,
					envName, documentId, null);
		}catch(Exception se) {
			logger.error(loggerMsgStr, validationType, se.getMessage());
			StatusBSO.insertStatusForRequestId(requestId, validationType, Constants.RequestStatus.DEADLETTER, status,
					envName, documentId, se.getMessage());
			throw new CustomStatusException(Constants.ResponseStatus.HTTPSTATUS_400, se.getMessage());
		}
		return isCallback;
	}

	/**
	 * @param requestId
	 * @param envName
	 * @param callBackJson
	 * @param callBackUrl
	 * @param callBackApikey
	 * @param isCallbacked
	 * @return
	 * @throws CustomStatusException 
	 * @throws Exception
	 */
	private void sendResponseAndInsertStatusForReqId(String requestId, String envName, String callBackJson,
			String callBackUrl, String callBackApikey) throws CustomStatusException {
		try {
			CallBackManager.sendResponseViaCallBackUrl(
					callBackJson, callBackUrl, RequestConstants.POST, callBackApikey);
			StatusBSO.insertStatusForRequestId(requestId, validationType, Constants.RequestStatus.DELIVEREDSUCCESSFULLY, status,
					envName, documentId, null);
		}catch(Exception se) {
			logger.error(loggerMsgStr, validationType, se.getMessage());
			StatusBSO.insertStatusForRequestId(requestId, validationType, Constants.RequestStatus.DEADLETTER, status,
					envName, documentId, se.getMessage());
			throw new CustomStatusException(Constants.ResponseStatus.HTTPSTATUS_400, se.getMessage());
		}
	}

}