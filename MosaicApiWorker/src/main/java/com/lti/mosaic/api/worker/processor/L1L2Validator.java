package com.lti.mosaic.api.worker.processor;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.fasterxml.jackson.databind.JsonNode;

import in.lnt.constants.Constants;
import in.lnt.exceptions.CustomStatusException;
import in.lnt.validations.FormValidator;

/**
 * 
 * @author rushi
 *
 */
public class L1L2Validator extends BaseValidator {

  private static final Logger logger = LoggerFactory.getLogger(L1L2Validator.class);

  public L1L2Validator(Map<String, String> map) {
    super(map);
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.lti.mosaic.api.worker.processor.BaseValidator#processJsonNode(com.fasterxml.jackson.
   * databind.JsonNode)
   */
  @Override
  public JsonNode process(Object inputJsonNode) throws CustomStatusException {
	  logger.info(">> L1L2Validator{}","{}");
	  FormValidator formValidator = new FormValidator();
	  JsonNode performL1L2Validations = null;
	  try {
	  performL1L2Validations = formValidator.perform_L1_L2Validations((JsonNode) inputJsonNode, false, true);
	  } catch (Exception e) {
		  StringWriter stringWriter = new StringWriter();
			e.printStackTrace(new PrintWriter(stringWriter));
			String stackTrace = stringWriter.toString();
			logger.error("Exception occured in L1L2Validator  stak trace is: "
			 + stackTrace +" message:" + e.getMessage()+ " cause:" +e.getCause());
	   throw new CustomStatusException(Constants.HTTPSTATUS_400, e.getMessage());
	  }
	  return performL1L2Validations;
  }


}
