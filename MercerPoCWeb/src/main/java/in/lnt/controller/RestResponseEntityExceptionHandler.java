package in.lnt.controller;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.convert.ConversionFailedException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.OptimisticLockingFailureException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.support.ErrorMessage;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import in.lnt.exception.CustomAPIException;


 /**
  *@author Sarang Gandhi
  *@since  Sept 2017 
  *@version 1.0 
  */
/**
 * REST exception handlers defined at a global level for the application
 */
@ControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {
	private static final Logger log = LoggerFactory.getLogger(RestResponseEntityExceptionHandler.class);

    /**
     * Catch all for any other exceptions...
     */
    @ExceptionHandler({ Exception.class })
    public ResponseEntity<ErrorMessage> handleAnyException(Exception e) {
        return errorResponse(e, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  
    

    @ExceptionHandler({ JSONException.class })
    public ResponseEntity<ErrorMessage> handleAnyException(JSONException e) {
        return errorResponse(e, HttpStatus.BAD_REQUEST);
    }
    
    @ExceptionHandler({ CustomAPIException.class })
    public ResponseEntity<ErrorMessage> handleAnyException(CustomAPIException e) {
        return errorResponse(e, HttpStatus.BAD_REQUEST);
    }
    
    /**
     * Handle failures commonly thrown from code
     */
    @ExceptionHandler({ InvocationTargetException.class, IllegalArgumentException.class, ClassCastException.class,
            ConversionFailedException.class})
    public ResponseEntity handleMiscFailures(Throwable t) {
        return errorResponse(t, HttpStatus.BAD_REQUEST);
    }

    /**
     * Send a 409 Conflict in case of concurrent modification
     */
    @ExceptionHandler({  OptimisticLockingFailureException.class,
            DataIntegrityViolationException.class })
    public ResponseEntity handleConflict(Exception ex) {
        return errorResponse(ex, HttpStatus.CONFLICT);
    }

    
    /**
     * Send a 409 Conflict in case of concurrent modification
     */
    @ExceptionHandler({  IOException.class,   FileNotFoundException.class })
    public ResponseEntity handleConflict(IOException ex) {
        return errorResponse(ex, HttpStatus.NOT_FOUND);
    }
    
    @ExceptionHandler(value = { HttpClientErrorException.class })
    protected ResponseEntity<Object> handleConflict(final RuntimeException ex, final WebRequest request) {
        if (ex instanceof HttpClientErrorException)
        {
            final HttpClientErrorException restEx = (HttpClientErrorException) ex;
            return handleException(restEx, request);
        }
        return handleExceptionInternal(ex, "Internal server error",
                new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR, request);
    }
    
    /**
     * 
     * @param throwable
     * @param status
     * @return ResponseEnity for the error messages.
     */
    protected ResponseEntity<ErrorMessage> errorResponse(Throwable throwable,HttpStatus status) {
        if (null != throwable) {
            log.error("error caught: " + throwable.getMessage(), throwable);
            return response(new CustomAPIException(status.value(),throwable.getMessage()), status);
        } else {
            log.error("unknown error caught in RESTController, {}", status);
            return response(null, status);
        }
    }

     /**
      * 
      * @param body
      * @param status
      * @return ResponseEntity for generic types messages.
      */
    protected <T> ResponseEntity<T> response(CustomAPIException body, HttpStatus status) {
        log.debug("Responding with a status of {}", status);
        ObjectMapper mapperObj = new ObjectMapper();
        String jsonStr = "";
        try {
        	mapperObj.configure(SerializationFeature.WRAP_ROOT_VALUE,true);
			mapperObj.writer().withRootName("errors");
			
			jsonStr = mapperObj.writeValueAsString(body);
			
		} catch (JsonProcessingException ex) {
			log.error("error caught: {}",ex.getMessage());
		} catch (IOException ex) {
			log.error("error caught: {}",ex.getMessage());
		}
        return new ResponseEntity(jsonStr.substring(0, jsonStr.indexOf("stackTrace")-1)+jsonStr.substring(jsonStr.indexOf("\"message\"")),status);
    }
    
    /**
     * 
     * @return Throwable object
     */
	public synchronized Throwable  fillInStackTrace()
	{
	return new Throwable();
	}
}