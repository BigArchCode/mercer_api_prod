package in.lnt.controller;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.fileupload.util.Streams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import in.lnt.constants.Constants;
import in.lnt.exceptions.CustomStatusException;
import in.lnt.parser.CSVParser;
import in.lnt.utility.constants.ErrorCacheConstant;
import in.lnt.utility.general.Cache;
import in.lnt.utility.general.JsonUtils;
import in.lnt.utility.poi.POIUtility;
import in.lnt.validations.FormValidator;

/**
 * Handles requests for the application file upload requests
 */
@Controller
public class FileUploadController {

	private static final Logger logger = LoggerFactory.getLogger(FileUploadController.class);

	// upload settings
	private static final int MEMORY_THRESHOLD = 1024 * 1024 * 3; // 3MB
	private static final int MAX_FILE_SIZE = 1024 * 1024 * 2000; // 2GB
	private static final int MAX_REQUEST_SIZE = 1024 * 1024 * 2500; // 2.5GB
	ArrayList<String> paramValueList=null;
	public static ObjectMapper mapper = new ObjectMapper();

	/**
	 * For Input as Files : MetaData as JSON and data as CSV or XLSX Only.
	 * 
	 * @param request JSON and CSV or XLSX File
	 * @param response JSON
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/uploadMultipleFile", method = RequestMethod.POST, produces = "application/json")
	public void uploadMultipleFileHandler(HttpServletRequest request, HttpServletResponse response) {
	
		logger.info(" Entering into uploadMultipleFileHandler method for json,xlsx and csv {}",request);
		long startTime = System.currentTimeMillis();
		
		ObjectNode errNode = null;
		FileItem fileJSON = null;
		List<InputStream> inputStreams = new ArrayList<>();
		String entitiesJson = "";
		HashMap<String, Object> compareFieldsMap = null;
		JSONObject validationDataObject = null;
		Map<String, Object> _L0_output = new HashMap<>();

		try {
			errNode = mapper.createObjectNode();
			Map<String, Object> parsedData = parseCsvRequest(request, response);
			inputStreams = (List<InputStream>) parsedData.get(Constants.INPUT_STREAMS);
			entitiesJson = (String) parsedData.get(Constants.ENTITIES_JSON);
			fileJSON = (FileItem) parsedData.get(Constants.FILE_JSON);
			_L0_output = performL0ValidationForFileRequest(response, parsedData, inputStreams, entitiesJson,
					fileJSON);
			if(_L0_output!=null)
			{
			compareFieldsMap = (HashMap<String, Object>) _L0_output.get(Constants.COMPARE_FIELDS_MAP);
			if (compareFieldsMap != null && compareFieldsMap.get(Constants.RESULTNODE) != null) {
				if (compareFieldsMap.get(Constants.RESULTNODE).equals(ErrorCacheConstant.ERR_055))
					throw new CustomStatusException(Constants.HTTPSTATUS_400,
							Cache.getPropertyFromError(ErrorCacheConstant.ERR_055));
				else {
					errNode = downloadDataForCompareField(response, errNode, compareFieldsMap);
				}
			} else {
				validationDataObject = new JSONObject(Streams.asString(fileJSON.getInputStream(), Constants.UTF_8));
				entitiesJson = validationDataObject.get(Constants.ENTITIES).toString();
				// Validation framework will be called here.
				validateForm(entitiesJson, response, (String) _L0_output.get(Constants.CSV_JSON_STRING), false,
						(LinkedHashMap<String, String>) _L0_output.get(Constants.XSL_JSON_MAP),
						(String) compareFieldsMap.get(Constants.SHEET_MATCHING_FLAG), true);
			}
			}

		}  catch (CustomStatusException cse) {

			errNode = JsonUtils.updateErrorNode(cse, errNode);
			downLoadData(response, errNode.toString().getBytes(), cse.getHttpStatus());
		}catch (Exception e) {
				
			logger.error("Exception occured in uploadMultipleFileHandler..{}", e.getMessage());
			errNode = JsonUtils.updateErrorNodeForGenericException(errNode, e);
			downLoadData(response, errNode.toString().getBytes(), Constants.HTTPSTATUS_500);
		}
		finally{
		for (InputStream inputStream : inputStreams) {
			try {
				inputStream.close();
			} catch (Exception e) {
				logger.error("Exception occured in uploadMultipleFileHandler for closing inputStream..{}",e.getMessage());
				errNode = JsonUtils.updateErrorNodeForGenericException(errNode, e);
				downLoadData(response, errNode.toString().getBytes(), Constants.HTTPSTATUS_500);
			}
		  }
		logger.info(" Existing from   uploadMultipleFileHandler method for json,xlsx and csv and time taken to execute this method is {}", 
				(System.currentTimeMillis() - startTime));
		}
		
	}

	private ObjectNode downloadDataForCompareField(HttpServletResponse response, ObjectNode errNode,
			HashMap<String, Object> compareFieldsMap) {
		try {

			downLoadData(response, compareFieldsMap.get(Constants.RESULTNODE).toString().getBytes(),
					Constants.HTTPSTATUS_422);
		} catch (Exception e) {
			logger.error("Exception occured in uploadMultipleFileHandler for downLoadData {}" , e.getMessage());
			// Exception need to be propagate on API where empty
			// JSON response will be written instead of blank.
			errNode = JsonUtils.updateErrorNodeForGenericException(errNode, e);
			downLoadData(response, errNode.toString().getBytes(), Constants.HTTPSTATUS_500);
		}
		return errNode;
	}
	
	/**
	 * Upload multiple file using Spring Controller
	 ** 
	 * @param validationData
	 * @param request
	 * @param response
	 * @return
	 * @throws IOException,JSONException
	 *             
	 */

	@RequestMapping(value = "/uploadMultipleFile", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	public @ResponseBody void uploadJSONRequestHandler(@RequestBody String validationData, HttpServletRequest request,
			HttpServletResponse response) {

		logger.info(" Entering into uploadJSONRequestHandler {}",validationData);

		long startTime = System.currentTimeMillis();

		ObjectNode errNode = null;
		try {
			errNode = mapper.createObjectNode();
			String entitiesJson = parseJsonRequest(validationData);
			Map<String, String> empIdMap = new FormValidator().checkUniqueNessForEEID(entitiesJson,null);
			if (empIdMap.get(Constants.ISUNIQUE).equals("true"))
				validateForm(entitiesJson, response, null, false, null, null, false);
			else if (empIdMap.get(Constants.ISUNIQUE).equals(Constants.DUPLICATE))
				throw new CustomStatusException(Constants.HTTPSTATUS_400,
						Cache.getPropertyFromError(ErrorCacheConstant.ERR_023));
			else if (empIdMap.get(Constants.ISUNIQUE).equals(Constants.MISSING))
				throw new CustomStatusException(Constants.HTTPSTATUS_400,
						Cache.getPropertyFromError(ErrorCacheConstant.ERR_039));
		} catch (CustomStatusException cse) {

			errNode = JsonUtils.updateErrorNode(cse, errNode);
			downLoadData(response, errNode.toString().getBytes(), cse.getHttpStatus());
		} catch (Exception e) {
			logger.error("Exception occured in uploadMultipleFileHandler..{}", e.getMessage());
			errNode = JsonUtils.updateErrorNodeForGenericException(errNode, e);
			downLoadData(response, errNode.toString().getBytes(), Constants.HTTPSTATUS_500);
		} finally {
			logger.info(" Exiting from  uploadJSONRequestHandler and time taken to execute this method is {}",
					(System.currentTimeMillis() - startTime));
		}

	}

	public @ResponseBody void validateForm(String entitiesJson, 
			HttpServletResponse response, String csvJsonString, boolean isAggregateRequest,
			Map<String, String> xslJsonMap, String firstSheetName, boolean isMultiPartRequest ) throws JSONException, IOException, CustomStatusException, SQLException {
		
		logger.info(" Entering into validateForm method {}",entitiesJson);
		
		long startTime = System.currentTimeMillis();
		JsonNode data = null;
		try {
			mapper.createObjectNode();
			FormValidator formValidator = new FormValidator();
			data = formValidator.parseAndValidate(entitiesJson, csvJsonString, isAggregateRequest, xslJsonMap,
					firstSheetName, isMultiPartRequest); 
			if (data != null) {
				downLoadData(response, data.toString().getBytes(Constants.UTF_8), 200);
			}
		} catch(Exception e){
			logger.error(" Error in validateForm method and time taken toexecutethis method is{}", e);
		}finally {	
			logger.info(" Exiting from validateForm method and time taken to execute this method is {}",(System.currentTimeMillis()-startTime) );
		}	
	}

	public static void downLoadData(HttpServletResponse response, byte[] bs, int status) {
		InputStream is;
		logger.info(" Entering into downLoadData method {}",response);
		long startTime = System.currentTimeMillis();
		try(OutputStream os = response.getOutputStream()) {
			is = new ByteArrayInputStream(bs);
			response.setStatus(status);
			// MIME type of the file
			response.setContentType("application/json");
			// Response header
			response.setHeader("Content-Disposition", "attachment; filename=\"output.json\"");

			// Read from the file and write into the response
			byte[] buffer = new byte[1024];
			int len;
			while ((len = is.read(buffer)) != -1) {
				os.write(buffer, 0, len);
			}
			os.flush();
			is.close();
			
		} catch (Exception e) {
			logger.error("Exception occured in downloadData..,{}",e.getMessage());
		}finally {
			logger.info(" Existing  from downLoadData method and time taken to execute this method is {}", (System.currentTimeMillis() - startTime));
		}
	}

	/**
	 * 
	 * @param jsonLikeObject
	 * @return message.
	 */
	public static String isJSONValid(String jsonLikeObject) {

		String returnValue = "true";
		try {
			new JSONObject(jsonLikeObject);
		} catch (JSONException ex) {
			try {
				new JSONArray(jsonLikeObject);
			} catch (JSONException ex1) {
				returnValue = ex.getMessage();
			}
		}
		return returnValue;
	}

	public Map<String, Object> parseCsvRequest(HttpServletRequest request, HttpServletResponse response) throws Exception
			  {

		logger.info(" Entering into parseCsvRequest method {}",request );
		long startTime = System.currentTimeMillis();
		String entitiesJson = "";
		Map<String, Object> parsedData = new HashMap<>();
		List<InputStream> inputStreams = new ArrayList<>();
		JSONObject errorResponse = new JSONObject();
		Map<String, Object> workBookMap = null;
		
		try {
			
		if (!ServletFileUpload.isMultipartContent(request)) {
			// if not, we stop here
			PrintWriter writer;
				writer = response.getWriter();
				errorResponse.put("RESPONSE", "Error: Form must has enctype=multipart/form-data.");
				writer.println(errorResponse);
				writer.flush();
			return null;
		}

		// configures upload settings
		DiskFileItemFactory factory = new DiskFileItemFactory();
		// sets memory threshold - beyond which files are stored in disk
		factory.setSizeThreshold(MEMORY_THRESHOLD);
		// sets temporary location to store files
		factory.setRepository(new File(System.getProperty("java.io.tmpdir")));
		ServletFileUpload upload = new ServletFileUpload(factory);
		// sets maximum size of upload file
		upload.setFileSizeMax(MAX_FILE_SIZE);
		// sets maximum size of request (include file + form data)
		upload.setSizeMax(MAX_REQUEST_SIZE);
		FileItem fileJSON = null;
		FileItem fileDATA = null;
		String extn = "";
			// parses the request's content to extract file data
			List<FileItem> formItems = upload.parseRequest(request);
			if (formItems != null && formItems.size() > 0) {

				for (FileItem item : formItems) {

					extn = item.getName().substring(item.getName().lastIndexOf('.') + 1);
					if (extn != null && !extn.equals("") && (extn.equalsIgnoreCase("csv")
							|| extn.equalsIgnoreCase("json") || extn.equalsIgnoreCase("xlsx"))) {
						if (item.getFieldName().equals("MetaData")) {
							fileJSON = item;
						} else if (item.getFieldName().equals("Data")) {
							fileDATA = item;
						}

						// Check Metadata and Data file size

						if (fileDATA != null && fileDATA.getSize() == 0) {
							throw new CustomStatusException(Constants.HTTPSTATUS_400,Cache.getPropertyFromError(ErrorCacheConstant.ERR_001));
						} else if (fileJSON != null && fileJSON.getSize() == 0) {
							throw new CustomStatusException(Constants.HTTPSTATUS_400,Cache.getPropertyFromError(ErrorCacheConstant.ERR_002));
						}

						// Check if file is xlsx file then convert to csv else
						// skip
						if (item.getFieldName().equals("Data") && extn.toLowerCase().equals("xlsx")) {
							inputStreams.add(item.getInputStream());
							workBookMap = POIUtility.prseXslxFile(item,fileJSON);

						} else {
							inputStreams.add(item.getInputStream());
							parsedData.put(Constants.INPUT_STREAMS, item);
						}

					} else {
						throw new CustomStatusException(Constants.HTTPSTATUS_400,Cache.getPropertyFromError(ErrorCacheConstant.ERR_003));
					}
				}
			}


		parsedData.put(Constants.INPUT_STREAMS, inputStreams);
		parsedData.put(Constants.FILE_JSON, fileJSON);
		parsedData.put("fileDATA", fileDATA);
		parsedData.put(Constants.ENTITIES_JSON, entitiesJson);
		parsedData.put("workBookMap", workBookMap);
		} finally {
			logger.info(" Existing from parseCsvRequest method and time taken to execute this method is {}", (System.currentTimeMillis() - startTime));
		}
		
		return parsedData;

	}

	@SuppressWarnings("unchecked")
	public String parseJsonRequest(String validationData) throws CustomStatusException {
		
		logger.info(" Entering into parseJsonRequest method {}",validationData );
		long startTime = System.currentTimeMillis();
		
		String entitiesJson = "";
		String key = "";
		String entitiesKey = null;
		JSONObject validationDataObject = null;
		Iterator<String> it = null;
		String validatedSyntax = "false";

		validatedSyntax = isJSONValid(validationData); // Syntax checker..
		if (validatedSyntax.equalsIgnoreCase("true")) {
			validationDataObject = new JSONObject(validationData);
			it = validationDataObject.keys();
		} else {
			throw new CustomStatusException(Constants.HTTPSTATUS_400,Cache.getPropertyFromError(ErrorCacheConstant.ERR_004));
		}
		try {
			while (it != null && it.hasNext()) {
				key = it.next();
				if (key.equalsIgnoreCase(Constants.ENTITIES)) {
					entitiesKey = key;
				}
			}

			if (entitiesKey != null && validationDataObject.get(entitiesKey) != null) {

				entitiesJson = validationDataObject.get(entitiesKey).toString();

			} else {
				throw new CustomStatusException(Constants.HTTPSTATUS_400,Cache.getPropertyFromError(ErrorCacheConstant.ERR_004));
			}
		} catch (Exception e) {
			throw e;
		}finally {
			logger.info(" Existing from parseJsonRequest method and time taken to execute this method is {}", (System.currentTimeMillis() - startTime));
		}
		return entitiesJson;

	}

	@RequestMapping(value = "/validateAggregates", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	public @ResponseBody void performAggregation(@RequestBody String validationData, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		ObjectNode errNode = null;
		
		logger.info(" Entering into performAggregation method {}",validationData );
		long startTime = System.currentTimeMillis();
		
		try{
			String entitiesJson = parseJsonRequest(validationData);
			//PE : 2449 to check duplication of EEID start in case of entity data in request body
			Map <String,String> empIdMap=new FormValidator().checkUniqueNessForEEID(entitiesJson,null);
			if(empIdMap.get(Constants.ISUNIQUE).equals("true"))
			validateForm(entitiesJson, response, null, true, null, null, false);
			else if(empIdMap.get(Constants.ISUNIQUE).equals(Constants.DUPLICATE))
			throw new CustomStatusException(Constants.HTTPSTATUS_400,Cache.getPropertyFromError(ErrorCacheConstant.ERR_023));
			else if(empIdMap.get(Constants.ISUNIQUE).equals(Constants.MISSING))
			throw new CustomStatusException(Constants.HTTPSTATUS_400,Cache.getPropertyFromError(ErrorCacheConstant.ERR_039));
			}
		catch(CustomStatusException cse) {

			errNode = mapper.createObjectNode();			
			errNode = JsonUtils.updateErrorNode(cse,errNode);
			downLoadData(response, errNode.toString().getBytes(Constants.UTF_8), cse.getHttpStatus());
		}
		catch(Exception e)
		{
			logger.error("Exception occured in performAggregation :: {}", e.getMessage());	
			downLoadData(response, JsonUtils.updateErrorNodeForGenericException(errNode, e).toString().getBytes(Constants.UTF_8), Constants.HTTPSTATUS_500);
		}finally {
			logger.info(" Existing from performAggregation method and time taken to execute this method is {}", (System.currentTimeMillis() - startTime));
		}
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/validateAggregates", method = RequestMethod.POST, produces = "application/json")
	public void performAggregation(HttpServletRequest request, HttpServletResponse response) {

		logger.info(" Entering into performAggregation method {}", request );
		long startTime = System.currentTimeMillis();
		
		String entitiesJson = "";
		FileItem fileJSON = null;
		List<InputStream> inputStreams = new ArrayList<>();
		JSONObject validationDataObject = null;
		HashMap<String, Object> csvMap = null;
		String csvJsonString=null;
		ObjectNode errNode = null;
		try {
		Map<String, Object> parsedData = parseCsvRequest(request, response);
		inputStreams = (List<InputStream>) parsedData.get(Constants.INPUT_STREAMS);
		entitiesJson = (String) parsedData.get(Constants.ENTITIES_JSON);
		fileJSON = (FileItem) parsedData.get(Constants.FILE_JSON);
			if (inputStreams.size() == 2) {
				
				csvMap=CSVParser.parseFile(inputStreams.get(1));
				if(csvMap!=null && csvMap.get(Constants.IS_EEID_DUPLICATE)==null)
				{
				HashMap<String, Object> map = new HashMap<String, Object>();
					map.put("data", csvMap.get(Constants.ALL_RECORDS));
				ObjectMapper objMapper = new ObjectMapper();
					csvJsonString = objMapper.writeValueAsString(map);
				validationDataObject = new JSONObject(Streams.asString(fileJSON.getInputStream(), Constants.UTF_8));
				entitiesJson = validationDataObject.get(Constants.ENTITIES).toString();
				validateForm(entitiesJson, response, csvJsonString, true, null, null, true);
				}
				else if(csvMap!=null && csvMap.get(Constants.IS_EEID_DUPLICATE)!=null && csvMap.get(Constants.IS_EEID_DUPLICATE).equals("true"))
				{
					throw new CustomStatusException(Constants.HTTPSTATUS_400,Cache.getPropertyFromError(ErrorCacheConstant.ERR_023));
				}
			}
		} 
		catch(CustomStatusException cse) {

			errNode = mapper.createObjectNode();			
			errNode = JsonUtils.updateErrorNode(cse,errNode);
			try {
				downLoadData(response, errNode.toString().getBytes(Constants.UTF_8), cse.getHttpStatus());
			} catch (UnsupportedEncodingException e) {
				logger.error(Constants.UNSUPPORTED_ENCODING_EXCEPTION,"{}",e.getMessage());	
			}
		}catch (Exception e) {
			logger.error("Exception occured in performAggregation {}",e.getMessage());
			errNode = JsonUtils.updateErrorNodeForGenericException(errNode, e);
			try {
				downLoadData(response, errNode.toString().getBytes(Constants.UTF_8), Constants.HTTPSTATUS_500);
			} catch (UnsupportedEncodingException uee) {
				logger.error(Constants.UNSUPPORTED_ENCODING_EXCEPTION,"{}",uee.getMessage());	
			}
		}
		finally{
		for (InputStream inputStream : inputStreams) {
			try {
				inputStream.close();
			} catch (IOException e) {
				logger.error(" Exception occured in performAggregation while closing inputstream..{}",e.getMessage());
				errNode = JsonUtils.updateErrorNodeForGenericException(errNode, e);
				try {
					downLoadData(response, errNode.toString().getBytes(Constants.UTF_8), Constants.HTTPSTATUS_500);
				} catch (UnsupportedEncodingException uee) {
					logger.error(Constants.UNSUPPORTED_ENCODING_EXCEPTION,uee.getMessage());	
				}
			}
		}
		logger.info(" Existing from performAggregation method and time taken to execute this method is {}", (System.currentTimeMillis() - startTime));

		}
	}

	@SuppressWarnings("unchecked")
	public Map<String, Object> performL0ValidationForFileRequest(
			HttpServletResponse response, Map<String, Object> parsedData, List<InputStream> inputStreams,
			String entitiesJson, FileItem fileJSON) {
		JSONObject validationDataObject = null;
		HashMap<String, Object> xlsxMap = null;
		HashMap<String, Object> csvMap = null;
		LinkedHashMap<String, String> xslJsonMap = null;
		String csvJsonString = null;
		HashMap<String, Object> compareFieldsMap = null;
		FormValidator formValidator = new FormValidator();

		List<String> fileNames = null;

		ObjectMapper objMapper = new ObjectMapper();
		ArrayNode entitiesNode = null;

		JsonNode contextDataNode = null;
		StringBuilder coNameAndCtryCode = new StringBuilder();
		List<String> xlsSheetList = null;
		List<String> entitiesList = null;
		Map<String, String> empIdMap = null;
		ObjectNode errNode = null;
		String reqDataInJson = "";
		JSONObject reqDataJsonObject = null;
		// L0 output 
		Map<String, Object> zeroLevelOutput = null;
		
		try {
			errNode = mapper.createObjectNode();
			if (inputStreams.size() == 2) {

				FileItem fileItemAndFileDATA = (FileItem) parsedData.get("fileDATA");
				fileNames = Arrays.asList(fileJSON.getName(), fileItemAndFileDATA.getName());
				if (fileNames.get(1).substring(fileNames.get(1).lastIndexOf('.') + 1).equalsIgnoreCase("json")) {
					reqDataJsonObject = new JSONObject(Streams.asString(fileItemAndFileDATA.getInputStream(), Constants.UTF_8));
					reqDataInJson = reqDataJsonObject.toString();
				}

				// Code to write HDFS ends here.
				validationDataObject = new JSONObject(Streams.asString(fileJSON.getInputStream(), Constants.UTF_8));
				entitiesJson = validationDataObject.get(Constants.ENTITIES).toString();
				// PE : 2449 to check duplication of EEID start in case of
				// entity data in request body
				empIdMap = new FormValidator().checkUniqueNessForEEID(entitiesJson,null);
				if (empIdMap != null && empIdMap.get(Constants.ISUNIQUE) != null
						&& empIdMap.get(Constants.ISUNIQUE).equals(Constants.DUPLICATE)) {
					throw new CustomStatusException(Constants.HTTPSTATUS_400,
							Cache.getPropertyFromError(ErrorCacheConstant.ERR_023));
				} // done
				else if (empIdMap != null && empIdMap.get(Constants.ISUNIQUE) != null
						&& empIdMap.get(Constants.ISUNIQUE).equals(Constants.MISSING)) {
					throw new CustomStatusException(Constants.HTTPSTATUS_400,
							Cache.getPropertyFromError(ErrorCacheConstant.ERR_038));
				}

				xlsxMap = formValidator.convertXlsxToMap((Map<String, Object>) parsedData.get("workBookMap"),
						empIdMap.get("empIdColumn"));
				if (!reqDataInJson.isEmpty()) {
					//sheetMatchingFlag
					//PE_8395
					//Added for 9635 : duplicate EEID check in case of data as body with mapping
					Object missingValue = false;
					empIdMap = new FormValidator().checkUniqueNessForEEID(entitiesJson,reqDataInJson);
					if (empIdMap != null && empIdMap.get(Constants.ISUNIQUE) != null
							&& empIdMap.get(Constants.ISUNIQUE).equals(Constants.DUPLICATE)) {
						throw new CustomStatusException(Constants.HTTPSTATUS_400,
								Cache.getPropertyFromError(ErrorCacheConstant.ERR_023));
					} 
					if(empIdMap != null && empIdMap.get(Constants.ISUNIQUE) != null
							&& empIdMap.get(Constants.ISUNIQUE).equals(Constants.MISSING))
					{
						missingValue = true;
					}
					
					formValidator.performL0Validation(entitiesJson, reqDataInJson, false, null,
							null);
					zeroLevelOutput = new HashMap<>();
					compareFieldsMap =  new HashMap<>();
					compareFieldsMap.put(Constants.SHEET_MATCHING_FLAG, null);
					zeroLevelOutput.put(Constants.CSV_JSON_STRING, reqDataInJson);
					zeroLevelOutput.put(Constants.COMPARE_FIELDS_MAP, compareFieldsMap);
					zeroLevelOutput.put(Constants.XSL_JSON_MAP, xslJsonMap);
					zeroLevelOutput.put(Constants.MISSING_EEID,missingValue);
					 return zeroLevelOutput;
					// csvJsonString = reqDataInJson;s
				} else if (xlsxMap == null) {
					csvMap = CSVParser.parseFile(fileItemAndFileDATA.getInputStream(), true);
					
					if (csvMap != null && csvMap.get(Constants.IS_EEID_DUPLICATE) == null) {
						//PE-8690
						
						HashMap<String, Object> map = new HashMap<>();
						map.put("data", csvMap.get(Constants.ALL_RECORDS));
						ObjectMapper objectMapper = new ObjectMapper();
						csvJsonString = objectMapper.writeValueAsString(map);
					
					} else if (csvMap != null && csvMap.get(Constants.IS_EEID_DUPLICATE) != null
							&& csvMap.get(Constants.IS_EEID_DUPLICATE).equals("true")) {
						throw new CustomStatusException(Constants.HTTPSTATUS_400,
								Cache.getPropertyFromError(ErrorCacheConstant.ERR_023));
					}
					if(csvMap != null ){
						compareFieldsMap = formValidator.compareFields(
								(List<HashMap<String, String>>) csvMap.get(Constants.ALL_RECORDS),
								(List<String>) csvMap.get("header"), entitiesJson, null,csvMap.get("eeidColFlag"));
					}
					if (compareFieldsMap != null && compareFieldsMap.get(Constants.RESULTNODE) != null
							&& !compareFieldsMap.get(Constants.RESULTNODE).equals(ErrorCacheConstant.ERR_055)) {
						downLoadData(response, compareFieldsMap.get(Constants.RESULTNODE).toString().getBytes(Constants.UTF_8),Constants.HTTPSTATUS_422);
						return null;
					}
				}

				// PE : 2449 to check duplication of EEID in excel sheet
				else if (xlsxMap != null
						&& (xlsxMap.get(Constants.IS_EEID_DUPLICATE) == null || xlsxMap.get(Constants.IS_EEID_DUPLICATE).equals("false"))) {
					compareFieldsMap = formValidator.compareFields(null, null, entitiesJson, xlsxMap,true);
					xslJsonMap = formValidator.prepareJsonString(xlsxMap);
				}
				// PE : 2449 to check duplication of EEID in csv
				else if (xlsxMap != null && xlsxMap.get(Constants.IS_EEID_DUPLICATE) != null
						&& xlsxMap.get(Constants.IS_EEID_DUPLICATE).equals("true")) {
					throw new CustomStatusException(Constants.HTTPSTATUS_400,
							Cache.getPropertyFromError(ErrorCacheConstant.ERR_023));
				}

				entitiesNode = objMapper.readValue(entitiesJson, ArrayNode.class);

				// For CSV data
				if (csvJsonString != null && !csvJsonString.equals("")) {
					if (entitiesNode.size() > 1) {
						downLoadData(response, csvJsonString.getBytes(Constants.UTF_8), Constants.HTTPSTATUS_422);
						return null;
					}
				} else if (xslJsonMap != null) {
					if (compareFieldsMap != null && compareFieldsMap.get(Constants.RESULTNODE) != null
							&& !compareFieldsMap.get(Constants.RESULTNODE).equals(ErrorCacheConstant.ERR_055)) {
						downLoadData(response, compareFieldsMap.get(Constants.RESULTNODE).toString().getBytes(Constants.UTF_8),
								Constants.HTTPSTATUS_422);
						return null;
					}
					// logger.debug("response sent successfully..");)
					if (entitiesNode.size() != xslJsonMap.size()) {
						paramValueList = new ArrayList<>();
						paramValueList.add("" + entitiesNode.size());
						paramValueList.add("" + xslJsonMap.size());
						throw new CustomStatusException(Constants.HTTPSTATUS_400,
								Cache.getPropertyFromError(ErrorCacheConstant.ERR_033),
								Cache.getPropertyFromError(ErrorCacheConstant.ERR_033 + Constants.UNDERSCORE_PARAMS),
								paramValueList);

					} else if (entitiesNode.size() > 1 && xslJsonMap.size() > 1) {
						// C1_US,C2_PL ; X1_XX, X2_YY

						xlsSheetList = new ArrayList<String>(xslJsonMap.keySet());

						entitiesList = new ArrayList<String>();
						for (int i = 0; i < entitiesNode.size(); i++) {
							contextDataNode = entitiesNode.get(i).get(Constants.CONTEXTDATAKEY);

							coNameAndCtryCode.delete(0, coNameAndCtryCode.length());
							coNameAndCtryCode.append(contextDataNode.get("companyName").asText());
							coNameAndCtryCode.append("_");
							coNameAndCtryCode.append(contextDataNode.get("ctryCode").asText());
							entitiesList.add(coNameAndCtryCode.toString());
						}
						if (!xlsSheetList.containsAll(entitiesList)) {
							throw new CustomStatusException(Constants.HTTPSTATUS_400,
									Cache.getPropertyFromError(ErrorCacheConstant.ERR_037));
						}

					}
				}
				if(compareFieldsMap != null){
					formValidator.performL0Validation(entitiesJson, csvJsonString, false, xslJsonMap,
						(String) compareFieldsMap.get(Constants.SHEET_MATCHING_FLAG));
				}
			}
			Object missingValue=((null != xlsxMap) ? (xlsxMap.get(Constants.MISSING_EEID)) : false);
			zeroLevelOutput = new HashMap<>();
			zeroLevelOutput.put(Constants.COMPARE_FIELDS_MAP, compareFieldsMap);
			zeroLevelOutput.put(Constants.CSV_JSON_STRING, csvJsonString);
			zeroLevelOutput.put(Constants.XSL_JSON_MAP, xslJsonMap);
			zeroLevelOutput.put(Constants.MISSING_EEID, ((null != csvMap) ? csvMap.get(Constants.MISSING_EEID)
					: missingValue));
 		} catch (CustomStatusException cse) {
			errNode = JsonUtils.updateErrorNode(cse, errNode);
			try {
				downLoadData(response, errNode.toString().getBytes(Constants.UTF_8), cse.getHttpStatus());
			} catch (UnsupportedEncodingException uee) {
				logger.error(Constants.UNSUPPORTED_ENCODING_EXCEPTION,"{0}",uee.getMessage());	
			}
		} catch (Exception e) {
			logger.error(" General exception caught {}", e.getMessage());
			errNode = JsonUtils.updateErrorNodeForGenericException(errNode, e);
			try {
				downLoadData(response, errNode.toString().getBytes(Constants.UTF_8), Constants.HTTPSTATUS_500);
			} catch (UnsupportedEncodingException uee) {
				logger.error(Constants.UNSUPPORTED_ENCODING_EXCEPTION,"{0}",uee.getMessage());	
			}
		}
		return zeroLevelOutput;
	}
}