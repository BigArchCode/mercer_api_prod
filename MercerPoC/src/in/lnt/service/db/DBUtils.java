package in.lnt.service.db;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.lti.mosaic.derby.MercerDBCP;
import com.lti.mosaic.function.def.CustomFunctions;

import in.lnt.constants.Constants;

public class DBUtils {

  private static final Logger logger = LoggerFactory.getLogger(DBUtils.class);

  private static Map<String, List<String>> refTableMetaDataCache =  new HashMap<>();
  private static Map<String, HashMap<String, String>> rangeDetailsCache =  new HashMap<>();
  private static Map<String, Map<String, String>> refTableDataCache = new HashMap<>();
  
  private DBUtils() {}

  public static List<String> getRefTableMetaData(String tableName)
      throws SQLException, IOException {

    if (refTableMetaDataCache.containsKey(tableName)) {
      return refTableMetaDataCache.get(tableName);
    } else {
      List<String> list = new ArrayList<>();
      ResultSet rs = null;

      ResultSetMetaData rsmetaData = null;
      int columnCount = 0;
      //Build query using string builder
      StringBuilder query = new StringBuilder (" SELECT * FROM " ); 
      query.append(tableName);
      query.append(CustomFunctions.FIRST_ROW);
      
      long startTime = System.currentTimeMillis();
      try(  Connection connection = MercerDBCP.getDataSource().getConnection();
        	  Statement stmt = connection.createStatement();) {
      
    	rs = stmt.executeQuery(query.toString());
        rsmetaData = rs.getMetaData();

        columnCount = rsmetaData.getColumnCount();
        for (int i = 1; i <= columnCount; i++) {
          list.add(rsmetaData.getColumnName(i));
        }
				logger.debug(
						"Time taken for creating connection and getting result statement for getRefTableMetaData() in DBUtiils..{}",
						(System.currentTimeMillis() - startTime));
      } catch (SQLException ex) {
        logger.error("Exception occured in getRefTableMetaData..{}", ex.getMessage());
        throw ex;
      } finally {
        if (rs != null) {
          rs.close();
        }
      }
      refTableMetaDataCache.put(tableName, list);
      return list;
    }
  }

  public static Map<String, String> getRangeDetails(String query)
      throws SQLException, IOException {

    if (rangeDetailsCache.containsKey(query)) {
      return rangeDetailsCache.get(query);
    } else {
      HashMap<String, String> map = new HashMap<>();
      ResultSet rs = null;
      long startTime = 0;
      startTime = System.currentTimeMillis();
      try ( Connection  connection = MercerDBCP.getDataSource().getConnection(); 
      	    Statement stmt = connection.createStatement();){
      	  
        rs = stmt.executeQuery(query);
        while (rs.next()) {
          map.put(Constants.ERROR_MIN, rs.getString(Constants.ERROR_MIN));
          map.put(Constants.ERROR_MAX, rs.getString(Constants.ERROR_MAX));
          map.put(Constants.ALERT_MIN, rs.getString(Constants.ALERT_MIN));
          map.put(Constants.ALERT_MAX, rs.getString(Constants.ALERT_MAX));
        }
				logger.debug(
						"Time taken for creating connection and getting result statement for getRangeDetails() in DBUtiils..{}",
						(System.currentTimeMillis() - startTime));
      } catch (SQLException ex) {
        map.put("SQLException", ex.getMessage());
        logger.error(Constants.EXCEPTION_OCCURED_IN_GETRANGEDETAILS , ex.getMessage());
        throw ex;
      } finally {
        if (rs != null) {
          rs.close();
        }
      }
      rangeDetailsCache.put(query, map);
      return map;
    }
  }

  /**
   * @param tableName
   * @return Map with values from two columns from refrence table.
   * @throws SQLException
   * @throws IOException
   */
  public static Map<String, String> getRefTableData(String tableName)
      throws SQLException, IOException {
	  
    if (refTableDataCache.containsKey(tableName)) {
      return refTableDataCache.get(tableName);
    } else {
      logger.info("{}","Entering Inside  getRefTableData method");

      Map<String, String> dataMap = new HashMap<>();
      ResultSet rs = null;

      StringBuilder query= new StringBuilder(" SELECT * FROM ");
      query.append(tableName);
      long startTime = 0;
      startTime = System.currentTimeMillis();
      logger.info("query is :{}" , query);
      
      try (Connection  connection = MercerDBCP.getDataSource().getConnection();
    		  Statement  stmt = connection.createStatement();){

    	rs = stmt.executeQuery(query.toString());
        logger.info("rs is :{}" ,rs.getMetaData());
        // Assuming table contains 2 columns only.
        while (rs.next()) {
          dataMap.put(rs.getString("CTRY_CODE"), rs.getString("CTRY_NAME"));
        }
		logger.debug("Time taken for creating connection and getting result statement for getRefTableData() in DBUtiils..{}",
						(System.currentTimeMillis() - startTime));
        logger.info("dataMap is :{}" , dataMap);
      } catch (SQLException ex) {
        logger.error("Exception occured in getRefTableData..{}",ex.getMessage());
        throw ex;
      } finally {

        if (rs != null) {
          rs.close();
        }
      }
      logger.info("{}","Existing from  getRefTableData method");
      refTableDataCache.put(tableName, dataMap);
      return dataMap;
    }
  }

  public static Map<String, ArrayList<HashMap<String, Object>>> loadDataBaseInMemory()
      throws SQLException, IOException {

    long startTime = 0;

    HashMap<String, ArrayList<HashMap<String, Object>>> recordsMap = null;
    List<String> tableNames = null;
    String sql = null;
    ResultSetMetaData rsMd = null;
    int columnCount = 0;
    Object recrodObj = null;
    boolean isRecordExist = false;
    
    //Try with resources 
    try(Connection connection = MercerDBCP.getDataSource().getConnection();
    		Statement stmt = connection.createStatement();) {

      tableNames = new ArrayList<>(50);
      recordsMap = new HashMap<>();
      startTime = System.currentTimeMillis();
      logger.info(" DB Operation start time is {}" , startTime);
      
      // --- LISTING DATABASE TABLE NAMES ---
      tableNames = getAllTables();

      // --- LISTING DATABASE COLUMN NAMES AND RECORDS FROM EACH TABLE---
     
      for (String table : tableNames) {

        ArrayList<HashMap<String, Object>> tempArrayList = null;
        tempArrayList = new ArrayList<>();

        sql = " SELECT * FROM  " + table;
        try(ResultSet resultSet = stmt.executeQuery(sql);){
        rsMd = resultSet.getMetaData();
        columnCount = rsMd.getColumnCount();
        tempArrayList.clear();
        while (resultSet.next()) {
          HashMap<String, Object> tempMap = null;
          tempMap = new HashMap<>();

          isRecordExist = true;
          for (int i = 1; i <= columnCount; i++) {
            recrodObj = resultSet.getObject(i);
            tempMap.put(rsMd.getColumnName(i), recrodObj);
          }
          tempArrayList.add(tempMap);
        }
        if (isRecordExist) {
          recordsMap.put(table, tempArrayList);
        }
      }
      logger.info(" DB Operation end time is {}",  (System.currentTimeMillis() - startTime));
      }
    } catch (SQLException ex) {
      logger.error("Exception occured in getRefTableData..{}" , ex.getMessage());
      throw ex;

    } 
    return recordsMap;
  }

  public static List<String> getAllTables() throws SQLException {

    DatabaseMetaData dbMd = null;
    ResultSet resultSet = null;
    Connection connection = null;
    ArrayList<String> tableNames = new ArrayList<>();
    // --- LISTING DATABASE TABLE NAMES ---
    try {
      // Take connection from the DB Connection pool.
      connection = MercerDBCP.getDataSource().getConnection();
      // Using sql connection get Meta Data for all tables in Database
      dbMd = connection.getMetaData();
      // Get tables listing in the resultset.
      resultSet = dbMd.getTables(null, null, "%", null);

      while (resultSet.next()) {
        resultSet.getString(2);
        tableNames.add(resultSet.getString(3));
      }
      logger.info("Table Name = {}" , tableNames);

    } catch (Exception ex) {
      logger.error("Exception occured in getTables..{}" , ex.getMessage());
    } finally {
      dbMd = null;
      if (resultSet != null) {
        resultSet.close();
      }
      if (connection != null) {
        try {
          connection.close();
        } catch (Exception e) {
          logger.error(Constants.LOG_CONNECTION_ERROR, e);
        }
      }
    }
    return tableNames;
  }

  
  /**
	 * 
	 * @param tableName
	 * @param columnValue
	 * @param columnName
	 * @return String
	 * PE-9734 mercer  JIRA id
 * @throws SQLException 
	 */
 
	public static String getDataFromDerbyTable(String runTimeQuery) throws SQLException {

		ResultSet rs = null;
		String dbOutPut = null;
		
		try (Connection connection = MercerDBCP.getDataSource().getConnection();
				Statement stmt = connection.createStatement();) {

			rs = stmt.executeQuery(runTimeQuery);
			while (rs.next()) {
				dbOutPut = rs.getString("REPLACEMENT");
			}
		} catch (SQLException ex) {
			logger.error(Constants.EXCEPTION_OCCURED_IN_GETRANGEDETAILS, ex);
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException ex) {
					logger.error(Constants.EXCEPTION_OCCURED_IN_GETRANGEDETAILS, ex);
				}
			}
		}
		return dbOutPut;
	}
	
	
}