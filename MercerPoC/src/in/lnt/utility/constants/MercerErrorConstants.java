package in.lnt.utility.constants;

import java.lang.reflect.Field;

public interface MercerErrorConstants {

	public static final String ERR_001 = "mosaic-error-message.csv_json_file_size";
	public static final String ERR_002 = "mosaic-error-message.json_file_size";
	public static final String ERR_003 = "mosaic-error-message.invalid_file_type";
	public static final String ERR_004 = "mosaic-error-message.invalid_json";
	public static final String ERR_005 = "mosaic-error-message.invalid_error_data";
	public static final String ERR_006 = "mosaic-error-message.miltiple_values";
	public static final String ERR_007 = "mosaic-error-message.variable_not_found";
	public static final String ERR_008 = "mosaic-error-message.expression_error";
	public static final String ERR_009 = "mosaic-error-message.argument_mismatch";
	public static final String ERR_010 = "mosaic-error-message.incomplete_expression";
	public static final String ERR_011 = "mosaic-error-message.params_mismatch";
	public static final String ERR_012 = "mosaic-error-message.expression_syntax";
	public static final String ERR_013 = "mosaic-error-message.params_type_mismatch";
	public static final String ERR_014 = "mosaic-error-message.invalid_expression";
	public static final String ERR_015 = "mosaic-error-message.invalid_params";
	public static final String ERR_016 = "mosaic-error-message.no_function_in_db";
	public static final String ERR_017 = "mosaic-error-message.invalid_aggregator_or_input";
	public static final String ERR_018 = "mosaic-error-message.params_length";
	public static final String ERR_019 = "mosaic-error-message.invalid_int";
	public static final String ERR_020 = "mosaic-error-message.wrong_data_while_aggregation";
	public static final String ERR_021 = "mosaic-error-message.runtime_error";
	public static final String ERR_022 = "Mosaic-error-message.service_overloaded";
	public static final String ERR_023 = "mosaic-error-message.duplicate_employeeid";
	public static final String ERR_024 = "mosaic-error-message.min_value_error";
	public static final String ERR_025 = "mosaic-error-message.min_value_alert";
	public static final String ERR_026 = "mosaic-error-message.max_value_alert";
	public static final String ERR_027 = "mosaic-error-message.max_value_error";
	public static final String ERR_028 = "mosaic-error-message.invalid_type_for_error";

	public static final String ERR_029 = "mosaic-error-message.field_required";
	
	public static final String ERR_032 = "mosaic-error-message.csv_file_entity_number";
	public static final String ERR_033 = "mosaic-error-message.entity_sheet_number_mismatch";
	public static final String ERR_034 = "mosaic-error-message.repeated_entity";
	public static final String ERR_035 = "mosaic-error-message.sheet_not_found";
	public static final String ERR_036 = "mosaic-error-message.entitiy_sheet_sequence_mismatch";
	public static final String ERR_037 = "mosaic-error-message.entitiy_ sheet_list_mismatch";
	public static final String ERR_038 = "mosaic-error-message.employeeid_info_not_found";
	public static final String ERR_039 = "mosaic-error-message.employeeid_missing";
	public static final String ERR_040 = "mosaic-error-message.unexpected_error";
	public static final String ERR_041 = "mosaic-error-message.required";
	public static final String ERR_042 = "mosaic-error-message.not_in_enum";
	public static final String ERR_043 = "mosaic-error-message.incorrect_format";
	public static final String ERR_044 = "mosaic-error-message.incorrect_result";
	public static final String ERR_045 = "mosaic-error-message.datatype_mismatch";
	public static final String ERR_046 = "mosaic-error-message.Please provide values";
	public static final String ERR_047 = "mosaic-error-message.message_from_request";
	public static final String ERR_048 = "mosaic-error-message.multiple_values";
	public static final String ERR_049 = "mosaic-error-message.incumbent_not_eligible";
	public static final String ERR_050 = "mosaic-error-message.no_display_label";
	public static final String ERR_051 = "mosaic-error-message.missing_table_name";
	public static final String ERR_052 = "mosaic-error-message.missing_column";
	public static final String ERR_053 = "mosaic-error-message.provide_one_of_values";
	public static final String ERR_054 = "mosaic-error-message.sequential_check";
	public static final String ERR_055 = "mosaic-error-message.invalid_excel_template";
	public static final String ERR_056 = "mosaic-error-message.missing_mappedCompany";
	public static final String ERR_057 = "mosaic-error-message.incumbent_not_eligible_error";
	public static final String ERR_058 = "mosaic-error-message.incumbent_eligible_alert";
	public static final String ERR_059 = "mosaic-error-message.system_autocorrect";
	public static final String ERR_060 = "mosaic-error-message.file_acceptance";
	public static final String ERR_061 = "mosaic-error-message.incorrect_integer_format";
	public static final String ERR_062 = "mosaic-error-message.incorrect_double_format";
	public static final String ERR_063 = "mosaic-error-message.incorrect_date_format";
	public static final String ERR_064 = "mosaic-error-message.missing_header";
	public static final String ERR_065 = "mosaic-error-message.duplicate_column";//8247
	public static final String ERR_066 = "mosaic-error-message.invalid_precision";//9490
	public static final String ERR_067 = "mosaic-error-message.wrong_year_format";//9841
	public static final String ERR_006_PARAMS = "valuesQty:varName:expression:";
	public static final String ERR_007_PARAMS = "varName:expression";
	public static final String ERR_033_PARAMS = "entitiesNum:sheetsNum";
	public static final String ERR_034_PARAMS = "companyCode:countryCode";
	public static final String ERR_035_PARAMS = "sheetName";
	public static final String ERR_040_PARAMS = "details";
	public static final String ERR_045_PARAMS = "key:value:datatype";
	public static final String ERR_041_PARAMS = "key";
	public static final String ERR_042_PARAMS = "value:values";
	public static final String ERR_043_PARAMS = "value";
	public static final String ERR_044_PARAMS = "result";
	public static final String ERR_048_PARAMS = "valuesQty:varName:expression";
	public static final String ERR_049_PARAMS = "code:displayLabel";
	public static final String ERR_050_PARAMS = "code:displayLabel:code1:displayLabell";
	public static final String ERR_052_PARAMS = "tableName";
	public static final String ERR_057_PARAMS = "displayLabel:baseQuestionLabel";
	public static final String ERR_058_PARAMS = "displayLabel";
	public static final String ERR_060_PARAMS = "_id:code";

	public default String getError(String key) {

		String value = null;
		try {
			Field field = MercerErrorConstants.class.getField(key);
			value = (String) field.get(MercerErrorConstants.class);
		} catch (Exception e) {
			return null;
		}
		return value;
	}
}
