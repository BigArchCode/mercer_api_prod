package in.lnt.constants;

public class Constants {
	private Constants(){
		
	}
	/**************** FOR REQUEST ****************/
	
	public static final int ENDPOINT_DEFAULT = -1;
	public static final int ENDPOINT_UPLOAD_MULTIPLE_FILE = 1;
	public static final String EXCEPTION_OCCURED_IN_GETRANGEDETAILS="Exception occured in getRangeDetails..{}";
	public static final String EXCEPTION_IS="Exception is {}";
	public static final String ITEMS="items";
	public static final String DISPLAY_NAME="displayName";
	public static final int ENDPOINT_VALIDATE_AGGREGATES = 2;
	public static final int ENDPOINT_CROSS_SECTION_CHECKS = 3;
	public static final int ENDPOINT_PERFORM_DEFAULT_ACTIONS = 4;
	public static final String COLUMNS = "columns";
	public static final String VALIDATIONS = "validations";
	public static final String VALIDATION_TYPE = "validationType";
	public static final String DATA_TYPE = "dataType";
	public static final String VALIDATION_ERRORS = "validationErrors";
	public static final String VALIDATION_RESULTS = "validationResults";
	public static final String EXPRESSION = "expression";
	public static final String ERROR_TYPE = "errorType";
	public static final String DEPENDENT_COLUMNS = "dependentColumns";
	public static final String DEPENDENT_COLUMN = "dependentColumn";
	public static final String ISMDAREQUEST = "isMDARequest";
	public static final String ERROR_GROUP = "errorGroup";
	public static final String PERSONAL = "Personal";
	public static final String DISPLAYLABEL = "displayLabel";
	public static final String AGGREGATE_DISPLAY_LABEL = "aggregateDisplayLabel";
	public static final String MEDIAN_PAY = "Median Pay";
	public static final String CATEGORY = "category";
	public static final String PARAMETER = "parameter";
	public static final String JOB_INVERSION = "job_inversion";
	public static final String ERROR = "ERROR";
	public static final String ALERT = "ALERT";
	public static final String REQUIRED = "REQUIRED";
	public static final String AUTOCORRECT = "AUTOCORRECT";
	public static final String PHONE = "PHONE";
	public static final String EMAIL = "EMAIL";
	public static final String AGGREGATE = "AGGREGATE";
	public static final String FIELD = "field";
	public static final String ANNUAL_BASE = "ANNUAL_BASE";
	public static final String MESSAGE = "message";
	public static final String MESSAGE_KEY = "messageKey";
	 public static final String JOB_INVERSION_ERROR_BASE_SALARY = "mosaic-error-message.job_inversion_base_salary_error";
	 public static final String JOB_INVERSION_ERROR_BASE_SALARY_INSTRUCTION = "mosaic-error-message.job_inversion_base_salary_error_instructions";
	public static final String JOB_INVERSION_ERROR_TOTAL_CASH = "mosaic-error-message.job_inversion_total_cash_error";
	public static final String JOB_INVERSION_ERROR_TOTAL_CASH_INSTRUCTION = "mosaic-error-message.job_inversion_total_cash_error_instructions"; 

	public static final String JOB_INVERSION_ERRORS = "Job Inversion Errors";
	public static final String CODE = "code";
	public static final String QUESTION_TYPE = "questionType";
	public static final String DROPDOWN = "dropdown";
	public static final String RADIO_BUTTONS = "radio_buttons";
	public static final String REPLACE_FROM = " and ";
	public static final String SPACE_AND_SPACE = REPLACE_FROM;
	public static final String REPLACE_TO = " && ";
	public static final String OTHERSECTIONDATAKEY = "otherSectionsData";
	public static final String CONTEXTDATAKEY = "contextData";
	public static final String ENTITIES = "entities";
	public static final String CONTEXTDATA_DATEFORMAT = ".dateFormat";
	public static final String CONTEXTDATA_NUMBERFORMAT = ".numberFormat";
	public static final String DATA = "data";
	public static final String SECTION_STRUCTURE = "sectionStructure";
	public static final String EXPRESSION_STRING = "EXPRESSIONSTRING";
	public static final String ENHANCEMENT = "ENHANCEMENT";
	public static final String SECTIONS = "sections";
	public static final String SECTIONCODE = "sectionCode";
	public static final String MAPPED_COLUMN_NAME = "mappedColumnName";
	public static final String BASE_FIELD = "baseField";
	public static final String MESSAGE_PARAMS = "messageParams";
	public static final String EXP_PATTERN = "(this|contextData)(.[a-zA-Z0-9_.!@#$%^&*]*)";
	public static String TAB_PATTERN_RANGE = "[^!A-Z\\s]+";
	public static final String INT_PATTERN = "^([+-]?[0-9]\\d*|0)$";
	public static final String DOUBLE_PATTERN = "\\d+\\.\\d+";
	public static final String DATATYPE_DOUBLE_PATTERN = "^[+-]?\\d+(\\.\\d+)?$";
	public static final String LONG_PATTERN = "^-?\\d{1,19}$";
	public static final String CURLY_BRACES = "\\{|\\}";
	public static final String SKIP = "skip";
	public static final String ID = "_id";
	public static final String MDA = "mda";
	public static final String EXECLUE_ROW = "exclude_row";
	public static final String CLEAR_VALUE = "clear_value";
	public static final String EXCLUDE_FLAG = "SYSTEM_EXCLUDE_EMPLOYEE"; //Value changed for PE-7771
	public static final String DUPLICATE = "Duplicate";
	public static final String MISSING = "Missing";
	public static final String EMPLOYEE_EEID = "YOUR_EEID";
	public static final String EEID_FORMAT = "yyyyMMddSSSSSmm";
	public static final String EEID_FORMAT2 = "yyyyMMddhh";
	public static final String ORIGINAL_VALUE = "OriginalValue";
	public static final String BLANK = "";
	public static final String DOT = ".";
	public static final String MINIMUM_ERROR = "minError";
	public static final String MAXIMUM_ERROR = "maxError";
	public static final String MINIMIUM_ALERT = "minAlert";
	public static final String MAXIMUM_ALERT = "maxAlert";
	public static final String VALIDATION_RANGE = "range";
	public static final String REFERENCE_TABLE_NAME = "referenceTableName";
	public static final String RANGE_VALIDATION_REF_TABLE = "rangeValidationRefTable";
	public static final String ERROR_MIN = "ERROR_MIN";
	public static final String ERROR_MIN_MSG = "value is less than minimum required Error value";
	public static final String ALERT_MIN = "ALERT_MIN";
	public static final String ALERT_MIN_MSG = "value is less than minimum required Alert value";
	public static final String ALERT_MAX = "ALERT_MAX";
	public static final String ALERT_MAX_MSG = "value is greater than maximum required Alert value";
	public static final String ERROR_MAX = "ERROR_MAX";
	public static final String ERROR_MAX_MSG = "value is greater than maximum required Error value";

	public static final int DATA_LEVEL1 = 0;
	/**************** FOR INTERNAL USE ****************/
	public static final String MISSING_EEID = "missing_eeid";

	
	public static final String HTTP_MEDIATYPE = "application/json";

	public static final String COMPARE_FIELDS_MAP = "compareFieldsMap";

	public static final String ALL_RECORDS = "allRecords";

	public static final String X_REQUEST_ID = "X-Request-ID";

	public static final String USER_AGENT = "User-Agent";

	public static final String UNSUPPORTED_ENCODING_EXCEPTION = "UnsupportedEncodingException : ";

	public static final String UTF_8 = "UTF-8";

	public static final String CSV_JSON_STRING = "csvJsonString";

	public static final String ENTITIES_JSON = "entitiesJson";

	public static final String FILE_JSON = "fileJSON";

	public static final String INPUT_STREAMS = "inputStreams";

	public static final String IS_EEID_DUPLICATE = "isEeidDuplicate";


	public static final String SHEET_MATCHING_FLAG = "sheetMatchingFlag";

	public static final String XSL_JSON_MAP = "xslJsonMap";


	
	/**************** ERRORS ****************/
	public static final String INVALID_DATATYPE_FOR_ERROR = "Invalid datatype found for errortype 'ERROR'";
	public static final String MANDATORY_VALIDATIONTYPE = "Mandatory attribute validationType is required.";
	/* CrossSectionChecks */
	public static final String ERROR_CANNOT_FIND_VARIABLE_IN_EXPR = "Cannot find variable '%s' in expression '%s'.";
	public static final String ERROR_N_VALUES_FOUND_FOR_VARIABLE_IN_EXPR = "%s values found for %s in expression %s";
	public static final String INVALID_FILE = "Invalid file type , only csv , json and xlsx expected.";
	public static final String INVALID_INPUT_JSON = "Invalid input JSON";

	// Logger constants..
	public static final String LOG_REQJSON = " Requested json....%s";
	public static final String LOG_METHOD_UPLOADMULTIPLEFILEHANDLER = "  >> uploadMultipleFileHandler:";
	public static final String LOG_METHOD_MERCERDEFINEDACTIONSHANDLER = "  >> mercerDefinedActionsHandler:";
	public static final String LOG_METHOD_PERFORMDEFAULTACTIONS = "  >> performDefaultActions:";
	public static final String LOG_METHOD_VALIDATEFORM = "  >> validateForm:";
	public static final String LOG_METHOD_DOWNLOAD = "  >> downLoadData:";
	public static final String LOG_PARSEANDVALIDATE = "  >> parseAndValidate:";
	public static final String LOG_PARSEJSONOBJECTMAP = "  >> parseJsonObjectMap:";
	public static final String LOG_VALIDATE = "  >> validate:";
	public static final String LOG_EXPRESSIONEVALUATOR = "  >> expressionEvaluator:";
	public static final String LOG_CHECKMANDATORYFIELDS = "  >> checkMandatoryFields:";
	public static final String LOG_CHECKDATATYPE = "  >> checkDataType:";
	public static final String LOG_CHECKVALIDATIONTYPEONEOF = "  >> checkValidationTypeOneOf:";
	public static final String LOG_PREPAREOUTPUTJSON = "  >> prepareOutputJson:";
	public static final String LOG_CHECKUNIQUENESSFOREEID = "  >> checkUniqueNessForEEID:";
	public static final String LOG_PREPAREEEIDAUTOCORRECTOBJECT = "  >> prepareEeidAutoCorrectObject:";
	public static final String LOG_GENERATEUNIQUEEID = "  >> generateUniqueEeid:";
	public static final String LOG_PARSECSVFILE = "  >> parseFile:";
	public static final String LOG_PARSEXSLXFILE = "  >> prseXslxFile:";
	public static final String LOG_PREPAREREFENCERANGEOBJECT = "  >> prepareRefernceRangeObject:";
	public static final String LOG_PERFORML0VALIDATION = "  >> performL0Validation:";
	public static final String LOG_CONVERXLSXTOMAP = "  >> convertXlsxToMap:";
	public static final String LOG_CROSSSECTIONCHECK = "  >> crossSectionCheck:";
	public static String LOG_WRITEDOCS = " : >> writeDocs()";
	public static String LOG_DOWNLOAD = " : >> download() ";

	public static final String INTIATE = "Executing..";
	public static final String EXIT = "Exiting..";

	public static final int HTTPSTATUS_422 = 422;
	public static final int HTTPSTATUS_400 = 400;

	public static final int HTTPSTATUS_500 = 500;
	public static final String ERROR_TABLE_NOTEXIST = " Missing  table name";
	public static final String ERROR_LOOKUP_COLUMN_MISSING_IN_DB = "Lookup column is missing in the  (%s) table for range validation.";
	// PE -5568
	public static final String ANSWER_YES = "Y";
	public static final String ANSWER_NO = "N";
	public static final String UNDERSCORE_PARAMS = "_PARAMS";
	public static final String OPTIONS = "options";  
	public static final String REFERENCETABLE = "referenceTable";
	public static final String DIMENSIONS = "dimensions";
	public static final String DIMENSION = "dimension";
	public static final String CHECKBOXES = "checkboxes";
	public static String RUN_LAST_ITERATION = "runLastIteration";
	public static String BREAK_ITERATION = "breakIteration";
	public static final String SEQUENTIAL_CHECK = "sequentialCheck";
	public static final String INT = "int";
	public static final String DATE = "date";//added for 8421
	public static final String INTEGER = "integer";
	public static final String BIGINT = "bigint";
	public static final String DOUBLE = "double";
	public static final String FLOAT = "float";
	public static final String NULL = "null";
	public static final String COMMA = ",";
	public static final String ZERO = "0";
	public static final String ENTITY_NAME_COLUMN_CODE="entityNameColumnCode";
	public static final String ENTITY_COUNTRY_COLUMN_CODE="entityCountryColumnCode";  
	public static final String MAPPED_COMPANY = "mappedCompany";
	public static final String INVALID_EXP = "invalid expression";
	public static final String PERCENTAGE = "percentage";
	public static final String HIDDEN = "hidden";
	public static final String COTEXT_COUNTRY_CODE = "contextData.ctryCode";
	public static final String COTEXT_INDUSTRY_SECTOR = "contextData.industry.sector";
	public static final String COTEXT_INDUSTRY_SUPERSECTOR = "contextData.industry.superSector";
	public static final String COTEXT_INDUSTRY_SUBERSECTOR = "contextData.industry.subSector";
	public static final String YES = "Y";
	

	//MONGO DB - REQUEST & RESPONSE
	public static final String REQUEST = "request";
	public static final String RESPONSE = "response";
	public static final String LOG_CONNECTION_ERROR="Error while closing connection {}";
	public static final String UNDERSCORE_ACFLAG="_ACflag";
	public static final String UNDERSCORE_REPLACE="_Replace";
	public static final String XLSXHEADERMAP="xlsxHeaderMap";
	public static final String COMPANYNAME="companyName";
	public static final String CTRYCDOE="ctryCode";
	public static final String XLSXDATAMAP="xlsxDataMap";
	public static final String FIRSTSHEETNAME="FirstSheetName";
	public static final String RESULTNODE="resultNode";
	public static final String ISUNIQUE="isUnique";
	public static final String COUNTRY="country";
	
	//Sonar changes start Sarang
    public static final String ATTENTION = "ATTENTION: ";
    public static final String DEFAULT_DECIMAL_FORMAT = "###,###.###";
    public static final String PATTERN = "pattern";
    public static final String THOUSANDSSEPARATOR = "thousandsSeparator";
    public static final String DECIMALSEPARATOR = "decimalSeparator";
    public static final String FALSE = "false";
    public static final String THIS = "this.";
    public static final String BOOL_IN_LOOKUP = "bool_in_lookup";
    public static final String OTHER_SECTIONSDATA_PERIOD = "otherSectionsData.";
    public static final String VALUES = "values";
    public static final String VALUE = "value";
    public static final String CTRY_CODE =  ".ctryCode";
    public static final String INDUSTRY_SECTOR = ".industry.sector";
    public static final String INDUSTRY_SUPERSECTOR = ".industry.superSector";
    public static final String INDUSTRY_SUBERSECTOR = ".industry.subSector";
    //Sonar changes end

	public static final String EXCEPTIONLITERAL="Exception is {}";
	public static final String EMPID_COL = "empIdColumn";
	public static final String EEIDINHEADER = "eeIdInHeader";
	public static final String MONEY = "money";//9490
	public static final String GUIDE = "Guide";//9868
	public static final String NAME = "name";
	public static final String COMPANY_COL = "companyCol";
	public static final String COUNTRY_COL = "countryCol";
	public static final String YEAR = "year";//9841
	public static final String RESULT = "result";
	
	//Columns
	//PE-9734
	public static final String COLUMN_EMP047 = "EMP_047";
	public static final String REGION_LOOKUP = "region_lookup";
}