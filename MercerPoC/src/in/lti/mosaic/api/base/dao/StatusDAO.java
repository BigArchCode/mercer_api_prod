package in.lti.mosaic.api.base.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import in.lti.mosaic.api.base.constants.Constants;
import in.lti.mosaic.api.base.exceptions.SystemException;
import in.lti.mosaic.api.base.mysql.MysqlOperations;
import in.lti.mosaic.api.base.beans.Status;

/**
 * @author rushi
 *
 */
public class StatusDAO {

  /**
   * @param requestId
   * @param environmentName
   * @return
   * @throws SystemException 
   */
	private StatusDAO() {
		throw new IllegalStateException("StatusDAO class");
	}
  public static Status fetchDocumentIdFromDbBasedOnRequestIdAndEnvironmentName(String requestId,
      String environmentName) throws SystemException {
    Map<String, Object> params = new HashMap<String, Object>();
    params.put(Constants.StatusConstants.REQUESTID, requestId);
    params.put(Constants.StatusConstants.STATUS, Constants.RequestStatus.DEADLETTER);
    params.put(Constants.StatusConstants.ENVIRONMENTNAME, environmentName);
    
    Status status = MysqlOperations.scanOneForQuery(Status.class, params);
    
    return status;
  }
  
  /**
   * @param requestId
   * @param environmentName
   * @return
   * @throws SystemException 
   */
  public static Status fetchStatusFromDbBasedOnRequestIdAndEnvironmentName(String requestId,
      String environmentName) throws SystemException {
    
    Status statusToReturn = new Status();
    
    Map<String, Object> params = new HashMap<String, Object>();
    params.put(Constants.StatusConstants.REQUESTID, requestId);
    params.put(Constants.StatusConstants.ENVIRONMENTNAME, environmentName);
    
    Long maxId = 0l;

    List<Status> listOfStatus = MysqlOperations.scanForQuery(Status.class, params);
    
    
    
    if (null != listOfStatus) {
      for(Status status : listOfStatus) {
        if(status.getId() > maxId) {
          maxId = status.getId();
          statusToReturn = status;
        }
      }
    } 
    
    return statusToReturn;
  }

}