/**
 * This class will maintain status of each request sumbitted to mosaic api
 * 
 */
package in.lti.mosaic.api.base.beans;

import in.lti.mosaic.api.base.mysql.Id;
import in.lti.mosaic.api.base.mysql.TableName;

/**
 * @author rushikesh
 *
 */

/*
  CREATE TABLE `status` ( `id` int(11) NOT NULL AUTO_INCREMENT, `request_id` varchar(50) DEFAULT
  NULL, `validation_type` varchar(50) DEFAULT NULL, `document_id` varchar(50) DEFAULT NULL, `status` varchar(50) DEFAULT NULL,
  `processing_status` int(11) DEFAULT NULL,
  `environment_name` varchar(50) DEFAULT NULL,`remarks` varchar(5000) DEFAULT NULL,
  `current_time_stamp` varchar(50) DEFAULT NULL, PRIMARY KEY (`id`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8
 */
@TableName(tableName = "status")
public class Status {

  @Id
  private Long id;
  private String request_id;
  private String validation_type;
  private String document_id;
  private String status;
  private Integer processing_status;
  private Long current_time_stamp;
  private String environment_name;
  private String remarks;


  /**
   * @return the id
   */
  public Long getId() {
    return id;
  }

  /**
   * @param id the id to set
   */
  public void setId(Long id) {
    this.id = id;
  }

  /**
   * @return the request_id
   */
  public String getRequest_id() {
    return request_id;
  }

  /**
   * @param request_id the request_id to set
   */
  public void setRequest_id(String requestId) {
    this.request_id = requestId;
  }
  
  /**
   * 
   * @return validation_type
   */
  public String getValidation_type() {
    return validation_type;
  }

  /**
   * @param validation_type the validation_type to set
   */
  public void setValidation_type(String validation_type) {
    this.validation_type = validation_type;
  }

  /**
   * @return the document_id
   */
  public String getDocument_id() {
    return document_id;
  }

  /**
   * @param document_id the document_id to set
   */
  public void setDocument_id(String document_id) {
    this.document_id = document_id;
  }

  /**
   * @return the status
   */
  public String getStatus() {
    return status;
  }

  /**
   * @param status the status to set
   */
  public void setStatus(String status) {
    this.status = status;
  }
  
  /**
   * @return the processing_status
   */
  public Integer getProcessing_status() {
    return processing_status;
  }

  /**
   * @param processing_status the processing_status to set
   */
  public void setProcessing_status(Integer processing_status) {
    this.processing_status = processing_status;
  }

  /**
   * @return the current_time_stamp
   */
  public Long getCurrent_time_stamp() {
    return current_time_stamp;
  }

  /**
   * 
   * @param currentTimeStamp
   */
  public void setCurrent_time_stamp(Long currentTimeStamp) {
    this.current_time_stamp = currentTimeStamp;
  }

  /**
   * @return the environment_name
   */
  public String getEnvironment_name() {
    return environment_name;
  }

  /**
   * @param environment_name the environment_name to set
   */
  public void setEnvironment_name(String environment_name) {
    this.environment_name = environment_name;
  }

  /**
   * @return the remarks
   */
  public String getRemarks() {
    return remarks;
  }

  /**
   * @param remarks the remarks to set
   */
  public void setRemarks(String remarks) {
    this.remarks = remarks;
  }

}
