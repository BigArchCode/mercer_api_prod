/**
 * 
 */
package in.lti.mosaic.api.base.exceptions;

import java.text.MessageFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import in.lnt.utility.constants.LoggerConstants;
import in.lti.mosaic.api.base.configs.Cache;
import in.lti.mosaic.api.base.loggers.ParamUtils;

/**
 * @author rushi
 *
 */
public class ExceptionsMessanger {
	private ExceptionsMessanger() {
		throw new IllegalStateException("ExceptionsMessanger class");
	}
  private static final Logger logger = LoggerFactory.getLogger(ExceptionsMessanger.class);

  public static void throwException(Throwable throwable, String messageType, Object... objs)
      throws SystemException {

    logger.debug(LoggerConstants.LOG_MAXIQAPI , " : >> throwException() {}"
        , ParamUtils.getString(throwable, messageType, objs));

    String errorMessage = ExceptionsMessanger.msg(Cache.getPropertyFromError(messageType), objs);

    logger.debug(LoggerConstants.LOG_MAXIQAPI , " : << throwException() {}"
        , ParamUtils.getString(throwable, messageType, objs));

    throw new SystemException(errorMessage, throwable);
  }

  public static String msg(String messageType, Object... objs) {
    logger.debug(
        LoggerConstants.LOG_MAXIQAPI , " : >> : msg() {}" , ParamUtils.getString(messageType, objs));

    MessageFormat format = new MessageFormat(messageType);

    String format2 = format.format(objs);

    logger
        .debug(LoggerConstants.LOG_MAXIQAPI , " : << String msg() {}" , ParamUtils.getString(format2));
    return format2;
  }

}
