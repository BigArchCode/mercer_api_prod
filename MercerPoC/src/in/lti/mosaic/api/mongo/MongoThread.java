package in.lti.mosaic.api.mongo;

import java.util.LinkedList;
import java.util.Queue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import in.lnt.utility.constants.CacheConstats;
import in.lnt.utility.general.Cache;

/**
 * @author rushikesh
 *
 */
public class MongoThread implements Runnable {

	private static final Logger logger = LoggerFactory.getLogger(MongoThread.class);

	private static Queue<MongoObject> queue = new LinkedList<MongoObject>();

	static {
		MongoThread mongoThread = new MongoThread();
		Thread thread = new Thread(mongoThread);
		thread.start();
	}

	public static synchronized void addToQueue(MongoObject mongoObject) {

		logger.debug(">> addToQueue >> {}", mongoObject.getRequestId());

		queue.add(mongoObject);

		logger.debug("<< addToQueue << {}", mongoObject.getRequestId());
	}

	@Override
	public void run() {
		while (true) {
			if(1==2){
				break;
			}
			MongoObject mongoObject = queue.poll();
			if (null == mongoObject) {
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					logger.error("Error in Thread sleep {} ", e.getMessage());
				}
			} else {
				MongoLoader.dumpDataToMongo(mongoObject);
			}
		}
	}

}
