{
    "entities": [
        {
            "sections": [
                {
                    "sectionCode": "incumbent_data",
                    "data": [
                        {
                            "A": null,
                            "B": "1",
                            "validationResults": [
                                {
                                    "expression": "this.C - this.D;",
                                    "errorType": "AUTOCORRECT",
                                    "validationType": "expression",
                                    "category": "cross_section",
                                    "message": "your title must be specified",
                                    "OriginalValue": "100",
                                    "field": "A"
                                }
                            ]
                        }
                    ]
                },
                {
                    "sectionCode": "company_data",
                    "data": [
                        {
                            "C": "1",
                            "D": "100"
                        }
                    ]
                }
            ],
            "contextData": {
                "companyId": "12345",
                "campaignId": "1",
                "grpCode": "1435",
                "cpyCode": "4444",
                "industry": {
                    "superSector": "DCG",
                    "subSector": "101",
                    "sector": "100"
                },
                "orgSize": 123,
                "ctryCode": "PL"
            }
        }
    ]
}