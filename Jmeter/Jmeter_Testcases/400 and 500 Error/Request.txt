{
  "entities": [
    {
      "contextData": {
        "campaignId": "1",
        "companyId": "12345",
        "grpCode": "1435",
        "cpyCode": "4444",
        "ctryCode": "PL",
        "industry": {
          "superSector": "DCG",
          "sector": "100",
          "subSector": "101"
        },
        "orgSize": 123
      },
      "sections": [
        {
          "sectionCode": "incumbent_data",
          "sectionStructure": {
            "columns": [
              {
                "code": "A",
                "displayLabel": "Organization Job Title",
                "dataType": "string",
                "questionType": "text",
                "validations": [
                  {
                    "errorType": "AUTOCORRECT",
                    "validationType": "expression",
                    "expression": "this.C - this.D;",
                    "category": "cross_section",
                    "message": "your title must be specified"
                  }
                ]
              },
              {
                "code": "B",
                "displayLabel": "Organization Job Title",
                "dataType": "string",
                "questionType": "text",
                "validations": [
                  
                ]
              }
            ]
          },
          "data": [
            {
              "A": "100",
              "B": "1"
            }
          ]
        },
        {
          "sectionCode": "company_data",
          "sectionStructure": {
            "columns": [
              {
                "code": "C",
                "displayLabel": "C",
                "dataType": "string",
                "questionType": "text",
                "category": "cross_section",
                "validations": [
                ]
              },{
                "code": "D",
                "displayLabel": "D",
                "dataType": "string",
                "questionType": "text",
                "category": "cross_section",
                "validations": [
                ]
              }
            ]
          },
          "data": [
            {
              "C": "1",
              "D":"100"
            }
          ]
        }
      ]
    }
  ]
}
