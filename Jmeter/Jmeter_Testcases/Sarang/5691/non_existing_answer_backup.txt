{
  "entities": [
    {
      "contextData": {
      	"uniqueIdColumnCode":"YOUR_EEID",
        "campaignId": "11111",
        "sectionId": "CompanyData",
        "grpCode": "1435",
        "cpyCode": "4444",
        "ctryCode": "PL",
        "industry": {
          "superSector": "CG",
          "sector": "100",
          "subSector": "101"
        },
        "orgSize": 20
      },
      "sectionStructure": {
        "columns": [
        	{
            "code": "YOUR_EEID",
            "displayLabel": "test",
            "dataType": "string",
            "questionType": "text",
            "validations": []
          },
          {
            "code": "QUESTION",
            "displayLabel": "test",
            "dataType": "string",
            "questionType": "text",
            "validations": [{
            	"errorType": "ERROR",
            	"errorGroup": "test",
            	"expression": "ISBLANK(this.QUESTION);",
            	"message": "test",
            	"validationType": "expression"
            }]
          }
        ]
      },
      "otherSectionsData": {},
      "data": [
      	{
      		"YOUR_EEID": "121",
      		"QUESTION": "value"
      	},
      	{
      		"YOUR_EEID": "122",
      		"QUESTION": null
      	},
		
		{
      		"YOUR_EEID": "124",
      		"QUESTION": ""
      	},
		
      	{
      		"YOUR_EEID": "123"
      	}
		
		
  		]
    }
  ]
}