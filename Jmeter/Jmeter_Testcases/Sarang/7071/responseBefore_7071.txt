{
    "entities": [
        {
            "data": [
                {
                    "EMP_119": null,
                    "EMP_117": null,
                    "EMP_238": "1",
                    "EMP_118": null,
                    "EMP_239": "7.5",
                    "POS_TITLE": "Long-Term Business Strategy & Planning - Senior Manager (M4)",
                    "EMP_283": null,
                    "EMP_240": "Y",
                    "EMP_284": null,
                    "EMP_281": "10",
                    "EMP_282": null,
                    "EMP_280": "Plan1",
                    "EMP_049": "49509",
                    "EMP_001": null,
                    "EMP_122": "A",
                    "EMP_123": "40",
                    "EMP_244": "1",
                    "EMP_043": "Y",
                    "EMP_285": null,
                    "EMP_044": "M",
                    "EMP_242": "500000",
                    "YOUR_TITLE": "DIRECTOR, BUSINESS TRANSFORMATION",
                    "EMP_129": "100",
                    "EMP_096": null,
                    "EMP_097": "30-10-16",
                    "EMP_290": null,
                    "EMP_136": null,
                    "EMP_134": null,
                    "EMP_054": null,
                    "EMP_098": null,
                    "EMP_055": null,
                    "EMP_099": null,
                    "EMP_058A": null,
                    "YOUR_EEID": "USabc0002",
                    "YOUR_LEVEL": null,
                    "EMP_261": "50000",
                    "EMP_061": null,
                    "EMP_260": "Y",
                    "EMP_026": "Chief Information Officer",
                    "EMP_067": null,
                    "EMP_100": null,
                    "EMP_109": null,
                    "EMP_273": null,
                    "EMP_038": "53",
                    "EMP_236": "24",
                    "YOUR_CODE": "320",
                    "POS_CLASS": null,
                    "EMP_039": "2014",
                    "EMP_116": null,
                    "EMP_036": "70",
                    "EMP_234": "50",
                    "EMP_278": "Y",
                    "EMP_037": "1961",
                    "EMP_114": null,
                    "EMP_279": null,
                    "EMP_034": null,
                    "EMP_233": "Yes",
                    "POS_CODE": "GMA.02.001.M40",
                    "EMP_032": null,
                    "YOUR_GRADE": null,
                    "validationErrors": [
                        {
                            "dependentColumns": "",
                            "expression": "this.EMP_129 + this.EMP_234;",
                            "clientInstructions": "",
                            "errorType": "AUTOCORRECT",
                            "parameter": "",
                            "validationType": "expression",
                            "_id": "5a0ee21c202289001578b6f7",
                            "message": "stored expr validation",
                            "category": "input_data",
                            "OriginalValue": "",
                            "field": "GZ134"
                        },
                        {
                            "field": "EMP_037",
                            "validationType": "expression",
                            "errorType": "ERROR",
                            "message": " current_year = YEAR(NOW());\n( current_year - NVL(this.EMP_037,0)) < LOOK_UP_CHECK(\"COUNTRY\",\"CTX_CTRY_CODE = contextData.ctryCode\",\"AGE_HIRE_MIN\")\n OR this.EMP_037 < (current_year - 66); ATTENTION: line 2:74 missing ',' at 'US'",
                            "category": "input_data"
                        },
                        {
                            "dependentColumns": "",
                            "expression": " current_year = YEAR(NOW());\n ISNOTBLANK(this.EMP_038) AND !(this.EMP_038 >= (current_year - 61) AND  this.EMP_038 <= (current_year));",
                            "clientInstructions": "Range 1958 - 2018.  Mercer will delete invalid values.",
                            "errorType": "ERROR",
                            "parameter": "",
                            "validationType": "expression",
                            "_id": "5a04228923212300164a48ab",
                            "message": "Year of Hire - Please review as the value is outside of the typical range.",
                            "category": "input_data",
                            "field": "EMP_038"
                        },
                        {
                            "field": "EMP_097",
                            "validationType": "dataTypeCheck",
                            "errorType": "ERROR",
                            "_id": "dataTypeDate",
                            "messageKey": "mosaic-error-message.incorrect_format",
                            "messageParams": {
                                "value": "date"
                            }
                        },
                        {
                            "field": "EMP_129",
                            "validationType": "expression",
                            "errorType": "ERROR",
                            "message": "ratio = 100*(this.EMP_129 /  this.EMP_131);\nEXP_SALINFLATION = NVL(LOOK_UP_CHECK(\"COUNTRY\",\"CTX_CTRY_CODE = this.contextData.ctryCode\",\"SALARY_INFLATION\"),1);\n\n(this.EMP_042 == \"N\" OR ISBLANK(this.EMP_042))\n AND this.EMP_131 > 0 AND  this.EMP_129 > 0 \n AND ( (ratio  - 100) < -5   OR ( (ratio - 100) > (EXP_SALINFLATION * 100 + 20 )) ) ; ATTENTION: line 2:70 missing ',' at 'US'",
                            "category": "input_data"
                        },
                        {
                            "dependentColumns": "",
                            "expression": "r1=0;\nvalue1 = 100*(this.EMP_234 / this.EMP_129);\nvalue2 = 1.1 * this.EMP_238;\n\nIF(ISBLANK(this.EMP_234) OR ISBLANK(this.EMP_238) OR ISBLANK(this.EMP_129)) \n{r1=false;}\nELSE\n{IF(value1>value2)\n{r1=true;}\nELSE\n{r1=false;}}\nr1;",
                            "clientInstructions": "Please review and update.  Mercer will leave data as submitted.  However, it may be marked as outlier or excluded from the survey as per data validation standards.",
                            "errorType": "ALERT",
                            "parameter": "",
                            "validationType": "expression",
                            "_id": "5a04264f23212300164a48b0",
                            "message": "Short-term Incentive (Actual) values are greater than Short-term Incentive (Maximum).",
                            "category": "input_data",
                            "field": "EMP_238"
                        }
                    ]
                },
                {
                    "EMP_119": null,
                    "EMP_117": null,
                    "EMP_238": "20",
                    "EMP_118": null,
                    "EMP_239": "5",
                    "POS_TITLE": "Legal & Compliance Management - Manager (M3)",
                    "EMP_283": null,
                    "EMP_240": "N",
                    "EMP_284": null,
                    "EMP_281": "10",
                    "EMP_282": null,
                    "EMP_280": "Plan1",
                    "EMP_049": "49509",
                    "EMP_001": null,
                    "EMP_122": "A",
                    "EMP_123": "40",
                    "EMP_244": "1",
                    "EMP_043": "Maybeeee",
                    "EMP_285": null,
                    "EMP_044": "Female",
                    "EMP_242": null,
                    "YOUR_TITLE": "MANAGER, COMPLIANCE",
                    "EMP_129": "87720",
                    "EMP_096": null,
                    "EMP_097": "30-10-16",
                    "EMP_290": null,
                    "EMP_136": null,
                    "EMP_134": null,
                    "EMP_054": null,
                    "EMP_098": null,
                    "EMP_055": null,
                    "EMP_099": null,
                    "EMP_058A": null,
                    "YOUR_EEID": "USabc0003",
                    "YOUR_LEVEL": null,
                    "EMP_261": null,
                    "EMP_061": null,
                    "EMP_260": "Y",
                    "EMP_026": "Director, Total Rewards",
                    "EMP_067": null,
                    "EMP_100": null,
                    "EMP_109": null,
                    "EMP_273": null,
                    "EMP_038": "42",
                    "EMP_236": "16",
                    "YOUR_CODE": "4067",
                    "POS_CLASS": null,
                    "EMP_039": "2010",
                    "EMP_116": null,
                    "EMP_036": "global",
                    "EMP_234": "14445",
                    "EMP_278": "Y",
                    "EMP_037": "1968",
                    "EMP_114": null,
                    "EMP_279": null,
                    "EMP_034": null,
                    "EMP_233": "Yes",
                    "POS_CODE": "LCA.02.001.M30",
                    "EMP_032": null,
                    "YOUR_GRADE": null,
                    "validationErrors": [
                        {
                            "dependentColumns": "",
                            "expression": "this.EMP_129 + this.EMP_234;",
                            "clientInstructions": "",
                            "errorType": "AUTOCORRECT",
                            "parameter": "",
                            "validationType": "expression",
                            "_id": "5a0ee21c202289001578b6f7",
                            "message": "stored expr validation",
                            "category": "input_data",
                            "OriginalValue": "",
                            "field": "GZ134"
                        },
                        {
                            "field": "EMP_036",
                            "errorType": "ERROR",
                            "message": "Please provide Geographic Scope of Role value."
                        },
                        {
                            "field": "EMP_037",
                            "validationType": "expression",
                            "errorType": "ERROR",
                            "message": " current_year = YEAR(NOW());\n( current_year - NVL(this.EMP_037,0)) < LOOK_UP_CHECK(\"COUNTRY\",\"CTX_CTRY_CODE = contextData.ctryCode\",\"AGE_HIRE_MIN\")\n OR this.EMP_037 < (current_year - 66); ATTENTION: line 2:74 missing ',' at 'US'",
                            "category": "input_data"
                        },
                        {
                            "dependentColumns": "",
                            "expression": " current_year = YEAR(NOW());\n ISNOTBLANK(this.EMP_038) AND !(this.EMP_038 >= (current_year - 61) AND  this.EMP_038 <= (current_year));",
                            "clientInstructions": "Range 1958 - 2018.  Mercer will delete invalid values.",
                            "errorType": "ERROR",
                            "parameter": "",
                            "validationType": "expression",
                            "_id": "5a04228923212300164a48ab",
                            "message": "Year of Hire - Please review as the value is outside of the typical range.",
                            "category": "input_data",
                            "field": "EMP_038"
                        },
                        {
                            "field": "EMP_043",
                            "errorType": "ERROR",
                            "message": "Please provide Same Incumbent 12 Months Prior value."
                        },
                        {
                            "field": "EMP_044",
                            "errorType": "ERROR",
                            "message": "Please provide Gender value."
                        },
                        {
                            "field": "EMP_097",
                            "validationType": "dataTypeCheck",
                            "errorType": "ERROR",
                            "_id": "dataTypeDate",
                            "messageKey": "mosaic-error-message.incorrect_format",
                            "messageParams": {
                                "value": "date"
                            }
                        },
                        {
                            "field": "EMP_129",
                            "validationType": "expression",
                            "errorType": "ERROR",
                            "message": "ratio = 100*(this.EMP_129 /  this.EMP_131);\nEXP_SALINFLATION = NVL(LOOK_UP_CHECK(\"COUNTRY\",\"CTX_CTRY_CODE = this.contextData.ctryCode\",\"SALARY_INFLATION\"),1);\n\n(this.EMP_042 == \"N\" OR ISBLANK(this.EMP_042))\n AND this.EMP_131 > 0 AND  this.EMP_129 > 0 \n AND ( (ratio  - 100) < -5   OR ( (ratio - 100) > (EXP_SALINFLATION * 100 + 20 )) ) ; ATTENTION: line 2:70 missing ',' at 'US'",
                            "category": "input_data"
                        }
                    ]
                }
            ],
            "contextData": {
                "uniqueIdColumnCode": "YOUR_EEID",
                "companyId": "59f00631dbd32d0015fef00e",
                "campaignId": "5a0180a406ff1e00156af3b3",
                "grpCode": "",
                "companyName": "ABC",
                "cpyCode": "",
                "industry": {
                    "superSector": "LS",
                    "subSector": "1040",
                    "sector": "106"
                },
                "sectionId": "EMP_DATA",
                "orgSize": {
                    "59f00631dbd32d0015fef00e": null
                },
                "ctryCode": "US"
            }
        }
    ]
}