def Autocorrection_Wrapper(QuestionMaster,ContextData,Incoming_Data1,SectionStructure2):
	Incoming_Data = Incoming_Data1
	try:
		## Generate the flags for Metadata Categorization
		DropDown1 = SectionStructure2[SectionStructure2.questionType == 'dropdown'][['code','displayLabel','options','dataType','questionType']]
		DropDown1['DD_Status'] = DropDown1.apply(lambda x: DD_Status_Flag(x['options']), axis= 1)
		DropDownMaster = DD_Label_Value(DropDown1)
		#DropDownMaster.to_csv('/home/maxiq/Autocorrect/Data/json/DropDownMaster16oct.csv', sep=',', encoding='utf-8', index  = False)
		QuestionMaster['Flag1'] = QuestionMaster.apply(lambda x: Flag_1(x['code'],x['dataType'],x['questionType'], DropDownMaster),axis=1)  
		QuestionMaster['Flag2'] = QuestionMaster.apply(lambda x: Flag_2(x['code'],x['dataType'],x['questionType'],x['Flag1'], DropDownMaster),axis=1)
		
		
		## Bucket the incoming columns for grouped processing
		Free_Text_Columns = sorted(QuestionMaster[QuestionMaster.questionType == 'text']['code'].unique().tolist())
		Double_Columns = sorted(QuestionMaster[QuestionMaster.questionType == 'double']['code'].unique().tolist())
		Percentage_Columns = sorted(QuestionMaster[QuestionMaster.questionType == 'percentage']['code'].unique().tolist())
		Integer_Columns = sorted(QuestionMaster[QuestionMaster.questionType == 'integer']['code'].unique().tolist())
		Date_Columns = sorted(QuestionMaster[QuestionMaster.questionType == 'date']['code'].unique().tolist())
		DD_Others_Columns = sorted(QuestionMaster[QuestionMaster.Flag2 == 'DD_Others']['code'].unique().tolist())
		DD_Range_Columns = sorted(QuestionMaster[QuestionMaster.Flag2 == 'DD_Range']['code'].unique().tolist())
		DD_Yes_No_Columns = sorted(QuestionMaster[QuestionMaster.Flag2 == 'DD_Yes_No']['code'].unique().tolist())
		DD_Country_Columns = sorted(QuestionMaster[QuestionMaster.Flag2 == 'DD_Country']['code'].unique().tolist())
		DD_Currency_Columns = sorted(QuestionMaster[QuestionMaster.Flag2 == 'DD_Currency']['code'].unique().tolist())
		DD_No_Act_Columns = sorted(QuestionMaster[(QuestionMaster.questionType == 'dropdown') & (QuestionMaster.Flag2 == 'False')  ]['code'].unique().tolist())
	
		Free_Text_Columns = filter(lambda x: x.startswith(tuple(Incoming_Data.columns)), Free_Text_Columns)
		Double_Columns = filter(lambda x: x.startswith(tuple(Incoming_Data.columns)), Double_Columns)
		Percentage_Columns = filter(lambda x: x.startswith(tuple(Incoming_Data.columns)), Percentage_Columns)
		#print(Percentage_Columns)
		Integer_Columns = filter(lambda x: x.startswith(tuple(Incoming_Data.columns)), Integer_Columns)
		Date_Columns = filter(lambda x: x.startswith(tuple(Incoming_Data.columns)), Date_Columns)
		DD_Others_Columns = filter(lambda x: x.startswith(tuple(Incoming_Data.columns)), DD_Others_Columns)
		DD_Range_Columns = filter(lambda x: x.startswith(tuple(Incoming_Data.columns)), DD_Range_Columns)
		DD_Yes_No_Columns = filter(lambda x: x.startswith(tuple(Incoming_Data.columns)), DD_Yes_No_Columns)
		DD_Country_Columns = filter(lambda x: x.startswith(tuple(Incoming_Data.columns)), DD_Country_Columns)
		DD_Currency_Columns = filter(lambda x: x.startswith(tuple(Incoming_Data.columns)), DD_Currency_Columns)
		DD_No_Act_Columns = filter(lambda x: x.startswith(tuple(Incoming_Data.columns)), DD_No_Act_Columns)
		
		print("Column Categorization")
		
		#Incoming_Data = pandas.read_csv('/home/maxiq/Autocorrect/Data/incoming/Raw_Data.csv' , sep='\t').astype(str)
		Incoming_Cols = Incoming_Data.columns
		for col in Incoming_Cols:
			Incoming_Data[''.join([col,"_Replace"])] = Incoming_Data[col]
			Incoming_Data[''.join([col,"_ACflag"])] = ""
					
		Incoming_Data = Incoming_Data[sorted(Incoming_Data.columns)]
		
		print("Spaces Stripper")
		for col in Incoming_Cols:
			Incoming_Data[''.join([col,"_Replace"])] = Incoming_Data[col].str.strip()
		
		print("Garbage Corrections")
		for col in Incoming_Cols:   
			Incoming_Data[''.join([col,"_Replace"])] = Incoming_Data.apply(lambda x: Garbage_Corrections(x[''.join([col,"_Replace"])]), axis = 1)
		
		print("Comma Remover")
		for col in DD_Range_Columns + Integer_Columns + Date_Columns:
		    Incoming_Data[''.join([col,"_Replace"])] = Incoming_Data.apply(lambda x: remove_comma(x[''.join([col,"_Replace"])]), axis = 1)
		
		print("Comma percentage Double")
		for col in Double_Columns + Percentage_Columns:
		    #print('in Comma remover')
		    Incoming_Data[''.join([col,"_Replace"])] = Incoming_Data.apply(lambda x: remove_comma_PercentageDouble(x[''.join([col,"_Replace"])]), axis = 1)    
		
		print("flagtext2int4")
		
		for col in Percentage_Columns + DD_Range_Columns + Double_Columns + Integer_Columns:
			Incoming_Data[''.join([col,"_Replace"])] = Incoming_Data.apply(lambda x: text2int(x[''.join([col,"_Replace"])]), axis = 1)
		
		print("Percentage formatter")
		
		for col in Percentage_Columns:
		    #print(col)
		    Incoming_Data[''.join([col,"_Replace"])] = Incoming_Data.apply(lambda x: Percent_To_DoubleFormatter(x[''.join([col,"_Replace"])]), axis = 1)
		
		print("currency 2 num")
		
		for col in DD_Range_Columns + Double_Columns + Integer_Columns:
			Incoming_Data[''.join([col,"_Replace"])] = Incoming_Data.apply(lambda x: currency2num(x[''.join([col,"_Replace"])]), axis = 1)
		
		print("Date Formatter")
		
		for col in Date_Columns:
			Incoming_Data[''.join([col,"_Replace"])] = Incoming_Data.apply(lambda x: DateFormater(x[''.join([col,"_Replace"])]), axis = 1)
		
		print("Y N Binary")
		
		for col in DD_Yes_No_Columns:
			Incoming_Data[''.join([col,"_Replace"])] = Incoming_Data.apply(lambda x: Yes_No_binary(x[''.join([col,"_Replace"])]), axis = 1)
		
		#print(Incoming_Data.head())	
		# Section to Read AutoCorrection_Cache from Mysql
		#print("AC Corrections")
        #
		db = MySQLdb.connect("hn1-mercer","root","maxiq","MercerACDB" )
		cursor = db.cursor()
		cursor.execute("SELECT * from Auto_Correction_Cache1")
		AC_DB = pandas.DataFrame(list(cursor.fetchall()), columns=map(lambda x:x[0], cursor.description))
		cursor.close()
		db.close()
		#AC_DB = pandas.read_csv('/home/maxiq/Autocorrect/Data/incoming/AutoCorrection_Cache.csv', sep = '|').astype(str)
		#AC_DB = pandas.read_csv('/home/maxiq/Autocorrect/Data/incoming/AC_DB_5.csv', sep = '|').astype(str)
		
		#for col in Double_Columns + Percentage_Columns + Integer_Columns + Date_Columns + DD_Others_Columns + DD_Range_Columns + DD_Yes_No_Columns + DD_Country_Columns + DD_Currency_Columns + DD_No_Act_Columns:
		#	Cache_Dict_String = str(AC_DB[(AC_DB.Flag == 'A') & (AC_DB.Code == col)][['Match','Replace']].set_index('Match').T.to_dict('list'))
		#	Incoming_Data[''.join([col,"_Replace"])] = Incoming_Data.apply(lambda x: Cache_Lookup(x[''.join([col,"_Replace"])],x[col] ,Cache_Dict_String ), axis = 1)
		#	Incoming_Data[''.join([col,"_ACflag"])] = Incoming_Data.apply(lambda x: Layer_Flag_Update_V1(x[''.join([col,"_Replace"])]), axis = 1)
		#	Incoming_Data[''.join([col,"_Replace"])] = Incoming_Data.apply(lambda x: Field_Flag_Chopper(x[''.join([col,"_Replace"])]), axis = 1)
		#
		print("DD Corrections")
		for col in DD_Others_Columns + DD_Country_Columns + DD_Currency_Columns: # + DD_Yes_No_Columns:
			Ref_Data = DropDownMaster[DropDownMaster.code == col][['displayName','value']]
			Ref_Data1 = str(Ref_Data.set_index('displayName').T.to_dict('list'))
			Incoming_Data[''.join([col,"_Replace"])] = Incoming_Data.apply(lambda x: Field_Resolver_27nov(x[col],x[''.join([col,"_Replace"])], x[col+'_ACflag'], Ref_Data1 ), axis = 1)
			Incoming_Data[''.join([col,"_ACflag"])] = Incoming_Data.apply(lambda x: Layer_Flag_Update_V1(x[''.join([col,"_Replace"])]), axis = 1)
			Incoming_Data[''.join([col,"_Replace"])] = Incoming_Data.apply(lambda x: Field_Flag_Chopper(x[''.join([col,"_Replace"])]), axis = 1)
		
		
		print("Range Formatter")
		## Range Assignment Columns Corrections
		#EMP_034_range = {
		#		250000:1, 
		#		500000:2,
		#		1000000:3,
		#		10000000000000000000000000000000000000000000:4
		#		
		#}
		#
		#EMP_396_range = {
		#		30000:36889, 
		#		50000:40205,
		#		75000:47648,
		#		100000:55914, 
		#		125000:64192,
		#		150000:72470,
		#		175000:80736, 
		#		200000:89014,
		#		250000:101419,
		#		300000:117963,
		#		350000:134507,
		#		400000:151051,
		#		500000:175873,
		#		600000:208961,
		#		700000:242061,
		#		800000:275149,
		#		80000000000000000000000000000000000000000:308237}
		#		
		#Incoming_Data['EMP_034_Replace'] = Incoming_Data.apply(lambda x: range_assigner(x['EMP_034_Replace'], str(EMP_034_range) ), axis = 1)
		
		#Incoming_Data['EMP_396_Replace'] = Incoming_Data.apply(lambda x: range_assigner(x['EMP_396_Replace'], str(EMP_396_range) ), axis = 1)
		## Layer Flag Update
		for col in Double_Columns + Percentage_Columns + Integer_Columns + Date_Columns + DD_Range_Columns + DD_No_Act_Columns :
			Incoming_Data[''.join([col,"_ACflag"])] = Incoming_Data.apply(lambda x: layer_Flag_update(x[''.join([col,"_Replace"])],x[col], x[''.join([col,"_ACflag"])] ), axis = 1)
		
		
		print("AC_DB update")
		ProcessDf = AC_DB
		for col in Incoming_Cols:
			ProcessDf1 = Incoming_Data[Incoming_Data[''.join([col,"_ACflag"])] == 'N'][[col,col+'_Replace',col+'_ACflag']]
			ProcessDf1['Code'] = col
			ProcessDf1['Region'] = None
			ProcessDf1['Country'] = None
			ProcessDf1['DisplayLabel'] = col
			ProcessDf1.columns = ['Match', 'Replace', 'Flag', 'Code', 'Region', 'Country', 'DisplayLabel']
			ProcessDf = ProcessDf1.append(ProcessDf)
		
		ProcessDf = ProcessDf.drop_duplicates()
		
		from pandas.io import sql
		from sqlalchemy import create_engine
		engine = create_engine("mysql+mysqldb://root:maxiq@10.4.0.9:3306/MercerACDB",  echo = False)
		ProcessDf.to_sql(name = 'Auto_Correction_Cache1', con = engine, if_exists = 'replace', index = False)
		engine.dispose()
		#ProcessDf.to_csv('/home/maxiq/Autocorrect/Data/incoming/AC_DB_5.csv', sep='|', encoding='utf-8', index  = False)
		#Incoming_Data.to_csv('/home/maxiq/Autocorrect/Data/incoming/Roger_That.csv', sep='|', encoding='utf-8', index  = False)
		return Incoming_Data
	except Exception as e:
		print('in wrapper Exception')
		print(e)
		Incoming_Cols = Incoming_Data1.columns
		for col in Incoming_Cols:
			Incoming_Data1[''.join([col,"_Replace"])] = Incoming_Data1[col]
			Incoming_Data1[''.join([col,"_ACflag"])] = 'X'
	return Incoming_Data1


	
	
## Import Fucntions
def DD_Label_Value(DropDown1):
	try:
		
		
		DropDownStructure1 = pandas.DataFrame(columns= ['code','displayLabel','DD_Flag1','value','displayName','dataType','questionType'])
		db = MySQLdb.connect("hn1-mercer","root","maxiq","mercerdb" )
		cursor = db.cursor()
		counter = 0
		for  i in DropDown1.options:
			
			if str(i) == 'nan':
				temp = pandas.DataFrame([[
							DropDown1.iloc[counter].code
						,DropDown1.iloc[counter].displayLabel
						,DropDown1.iloc[counter].DD_Status
						,''
						,''
						,DropDown1.iloc[counter].dataType
						,DropDown1.iloc[counter].questionType]
							]
						,columns=['code','displayLabel','DD_Flag1','value','displayName','dataType','questionType'])
				DropDownStructure1 = DropDownStructure1.append(temp)
				counter = counter + 1
			elif (i.keys()[0] == 'items'):
		
				code = DropDown1.iloc[counter].code
				displayLabel = DropDown1.iloc[counter].displayLabel
				DD_Status = DropDown1.iloc[counter].DD_Status
				dataType = DropDown1.iloc[counter].dataType
				questionType = DropDown1.iloc[counter].questionType
				for m in i.values()[0]:
					temp = pandas.DataFrame([[
								code
								,displayLabel
								,DD_Status
								,m['value']
								,m['displayName']
								,dataType
								,questionType]
								]
								,columns=['code','displayLabel','DD_Flag1','value','displayName','dataType','questionType'])
					DropDownStructure1 = DropDownStructure1.append(temp)
				counter = counter + 1
			
			else:
				Value_Column_Name = i.values()[0]['valueColumn']
				Ref_Table_Name = i.values()[0]['code']
				Display_Column_Name = i.values()[0]['displayNameColumn']
				Sql_String = 'select '+Display_Column_Name +','+Value_Column_Name+' from '+Ref_Table_Name
				cursor.execute(Sql_String)
				Ref_Data_Temp = pandas.DataFrame(list(cursor.fetchall()), columns=map(lambda x:x[0], cursor.description))
				Ref_Data_Temp.rename(columns={'CTRY_NAME': 'value', 'CTRY_CODE': 'displayName'}, inplace=True)
				Ref_Data_Temp['code'] = DropDown1.iloc[counter].code
				Ref_Data_Temp['displayLabel'] = DropDown1.iloc[counter].displayLabel
				Ref_Data_Temp['DD_Flag1'] = DropDown1.iloc[counter].DD_Status
				Ref_Data_Temp['dataType'] = DropDown1.iloc[counter].dataType
				Ref_Data_Temp['questionType'] = DropDown1.iloc[counter].questionType
				DropDownStructure1 = DropDownStructure1.append(Ref_Data_Temp)
				counter = counter + 1
		return DropDownStructure1 
		cursor.close()
		db.close()        
	except Exception as e:
		print('DD_Label_Value')
		print(e)
		


def Flag_1(code, datatype,questiontype,DropDownMaster):
	try:
		if ((datatype == 'string') & (questiontype == 'text')):
			return 'Freetext'
		elif ((datatype == 'string') & (questiontype == 'dropdown')):
			return DropDownMaster[DropDownMaster.code == code]['DD_Flag1'].unique()[0]
		elif ((datatype == 'double') & (questiontype == 'double')):
			return 'double'
		elif ((datatype == 'double') & (questiontype == 'percentage')):
			return 'percentage'
		elif ((datatype == 'int') & (questiontype == 'integer')):
			return 'integer'
		elif ((datatype == 'date') & (questiontype == 'date')):
			return 'date'
	except Exception as e:
		#print('Flag_1')
		print(e)
		return 'False'
	


def Flag_2(Code, Datatype,Questiontype,Flag1, DropDownMaster):
	try:
		if Flag1 not in ('Local'): 
			return 'False'
		CurrencyList = ['AED','AMD','ARS','AUD','AZN','BAM','BGN','BHD','BRL','BYR','CAD','CHF','CLP','CNY','COP','CRC','CZK',
						'DKK','DZD','EEK','EGP','EUR','GBP','GEL','GTQ','HKD','HRK','HUF','IDR','ILS','INR','IRR','JPY','KRW',
						'KWD','KZT','LTL','LVL']
		CountryList = ['Afghanistan','Algeria','Andorra','Angola','Argentina','Armenia','Australia','Austria','Azerbaijan',
						'Bahrain','Bangladesh','Barbados','Belarus','Belgium','Bermuda','Bolivia','Botswana','Brazil',
						'British Virgin Islands','Bulgaria','Cambodia','Cameroon','Canada','Cayman Islands','Chile','China',
						'Colombia','Congo','Costa Rica','Croatia','Cyprus','Czech Republic','Denmark','Dominican Republic',
						'Ecuador','Egypt']
		RangeListTemp1 = ['Less than $250K', '$250K < $500K', '$500K < $1MM', '$1MM and above']
		RangeListTemp2 = ['Up To R30,000 => 36889','R30,00 R50,000 => 40205','R50,00 R75,000 => 47648','R75,00 R100,000 => 55914',
						'R100,00 R125,000 => 64192','R125,00 R150,000 => 72470','R150,00 R175,000 => 80736',
						'R175,00 R200,000 => 89014','R200,00 R250,000 => 101419','R250,00 R300,000 => 117963',
						'R300,00 R350,000 => 134507','R350,00 R400,000 => 151051','R400,00 R500,000 => 175873',
						'R500,00 R600,000 => 208961','R600,00 R700,000 => 242061','R700,00 R800,000 => 275149',
						'More than R800,001 => 308237'] 
		DDValueList = sorted(DropDownMaster[DropDownMaster.code == Code]['value'].unique().tolist())
		DDValueListLen = len(DropDownMaster[DropDownMaster.code == Code]['value'].unique().tolist())
		if (('Y' in DDValueList ) & ('N' in DDValueList)):
			return 'DD_Yes_No'
		if (set(DDValueList[0:10])<set(CurrencyList)):
			return 'DD_Currency'
		if (set(DDValueList[0:10])<set(CountryList)):
			return 'DD_Country'
		if ((set(DDValueList[0:3])<set(RangeListTemp1)) | (set(DDValueList[0:4])<set(RangeListTemp2))):
			return 'DD_Range'
		return 'DD_Others'
	except Exception as e:
		print(' Flag 2')
		print(e)
		return 'DD_Others'


def DD_Status_Flag(options):
	try:
		if str(options) == 'nan':
			return 'False'
		if str(options).find('To be aligned with MJL') > 0:
			return 'MJL'
		else:
			return 'Local'
	except Exception as e:
		print('DD_Status_Flag')
		print(e)
		return 'False'



########Functon-1########## Garbage Corrections

def Garbage_Corrections(col_String):
	try:
		if col_String in ["not available", "n/a", "#value", "#N/A", " ", "na", "N/A", "nia", "NA", "DNA", "not applicable", "dna", "error", "confidential","", "nan"]:
			return None
		else:
			return col_String
	except Exception as e:
		print('Garbage_Corrections')
		print(e)
		return col_String
		

########Functon0########Layer Flag Update
def layer_Flag_update(Replace, original, Flag):
	try:
		if Flag == 'A':
			return 'A'
		if (Replace==original):
			return 'X'
		else:
			return 'N'
	except Exception as e:
		print('layer_Flag_update')
		print(e)
		return 'X'    
		
	####Functon1########TextToNum

def text2int (textnum, numwords={}):
	if textnum is None:
		return textnum
	try:
		if textnum:
			textnum = textnum.lower()
			#textnum = textnum.replace(" ", "")
			if not numwords:
				units = [
				"zero", "one", "two", "three", "four", "five", "six", "seven", "eight",
				"nine", "ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen",
				"sixteen", "seventeen", "eighteen", "nineteen",
				]
				tens = ["", "", "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety"]
				scales = ["hundred", "thousand", "million", "billion", "trillion"]
				numwords["and"] = (1, 0)
				for idx, word in enumerate(units):  numwords[word] = (1, idx)
				for idx, word in enumerate(tens):       numwords[word] = (1, idx * 10)
				for idx, word in enumerate(scales): numwords[word] = (10 ** (idx * 3 or 2), 0)
			ordinal_words = {'first':1, 'second':2, 'third':3, 'fifth':5, 'eighth':8, 'ninth':9, 'twelfth':12}
			ordinal_endings = [('ieth', 'y'), ('th', '')]
			textnum = textnum.replace('-', ' ')
			current = result = 0
			curstring = ""
			onnumber = False
			for word in textnum.split():
				if word in ordinal_words:
					scale, increment = (1, ordinal_words[word])
					current = current * scale + increment
					if scale > 100:
						result += current
						current = 0
					onnumber = True
				else:
					for ending, replacement in ordinal_endings:
						if word.endswith(ending):
							word = "%s%s" % (word[:-len(ending)], replacement)
					if word not in numwords:
						if onnumber:
							curstring += repr(result + current) + " "
						curstring += word + " "
						result = current = 0
						onnumber = False
					else:
						scale, increment = numwords[word]
						if scale > 1:
							current = max(1, current)
						current = current * scale + increment
						if scale > 100:
							result += current
							current = 0
						onnumber = True
			if onnumber:
				curstring += repr(result + current)
			return curstring.strip()
	except Exception as e:
		print('text2int')
		print(e)
		return 'X'    


########Functon2########Remove comma from numeric and currency columns

def remove_comma(val):
	if val is None:
		return val
	try:
		if val:
			return val.replace(",","")
		return None
	except Exception as e:
		print('remove_comma')
		print(e)
		return val
		
		
		
########Functon2.1########Remove comma from numeric and currency columns

def remove_comma_PercentageDouble(val):
	if val is None:
		return val
	try:
		if val:
			return val.replace(",",".")
		return None
	except Exception as e:
		print('remove_comma_PercentageDouble')
		print(e)
		return val
		

########Functon2.1########Data Type Formatters
       
def Percent_To_DoubleFormatter(val):
	if val is None:
		return val
	try:
		#print(val)
		
		return re.sub(r'[pP\%]\w*', '', val)
	except Exception as e:
		print('Percent_To_DoubleFormatter')
		print(e)
		return val


########Functon3########onverts currency to numeric values


def currency2num(text):
	if text is None:
		return text
	try:
		
		num_replace = {
		'b' : 1000000000,
		'm' : 1000000,
		'k' : 1000}
		
		if text: 
			text = re.sub('[!@#$]', '', text)
			text = text.replace('thousand','k').replace('thousands','k')
			text = text.replace('million','m').replace('millions','m')
			text = text.replace('billion','b').replace('billions','b')
			text = text.lower().strip()
			mult = 1.0
			try:
				while text[-1] in num_replace:
					mult *= num_replace[text[-1]]
					text1 = text[:-1]
					return '{0:.0f}'.format(float(text1) * mult)
			except ValueError:
				return text
		return text
	except Exception as e:
		print('Percent_To_DoubleFormatter')
		print(e)
		return text
		

########Functon4########Parses the Date in Specified Format

def DateFormater(Date_String):
	#print(Date_String)
	if Date_String is None:
		return(Date_String)
	
	try:
		if unicode(Date_String.replace(" ","")).isnumeric():
			Date_String = Date_String.replace(" ","")
			Date_String = unicode(Date_String)
			
			if (len(Date_String) == 4 and  int(Date_String) >= 1950 and int(Date_String) <= 2099):
				
				return datetime(year=int(Date_String), month=01, day=01).strftime('%m/%d/%Y')
				#return datetime(year=int(Date_String), month=01, day=01).strftime('%a, %d %b %Y %H:%M:%S')
			else:
				return xlrd.xldate_as_datetime(float(Date_String),1).strftime('%m/%d/%Y')
				#return xlrd.xldate_as_datetime(float(Date_String),1).strftime('%a, %d %b %Y %H:%M:%S')
		else:
			return parser.parse(Date_String, fuzzy=True).strftime('%m/%d/%Y')
			#return parser.parse(Date_String, fuzzy=True).strftime('%a, %d %b %Y %H:%M:%S')
	except Exception:
		#print('Date Converter')
		#print(e)
		return Date_String



########Functon5########Corrects all yes/No based binary Columns for level 1 sweep

def Yes_No_binary(Col_String):
	if Col_String is None:
		return(Col_String)
	try:
		if Col_String.lower() in ['yes','y','ye','yess','ya','yees','yyes']:
			return 'Y'
		elif Col_String.lower() in ['no','n','noo','nn','nno','non','none']:
			return 'N'
		elif is_abbrevation(Col_String.lower(), 'yyyyyyyyyyyyyyeeeeeeeeeeeeeeeeesssssssssssssssss'):
			return 'Y'
		elif is_abbrevation(Col_String.lower(), 'nnnnnnnnnnnnnnnnnnoooooooooooooooooooo'):
			return 'N'
		else:        
			return Col_String
	except Exception as e:
		print('Yes_No_binary')
		print(e)
		return Col_String
		
		

## Range Update Functions

def range_assigner(input, Range_Dict_String):
	if input is None:
		return(input)
	ranges_assetumgt_tps = eval(Range_Dict_String)
	try:
		input = float(input)
		for i in range(len(ranges_assetumgt_tps)):
			if input <= sorted(ranges_assetumgt_tps.keys())[0]:
				return(ranges_assetumgt_tps[sorted(ranges_assetumgt_tps.keys())[0]])
			elif sorted(ranges_assetumgt_tps.keys())[i] < input <= sorted(ranges_assetumgt_tps.keys())[i+1]:
				return(ranges_assetumgt_tps[sorted(ranges_assetumgt_tps.keys())[i+1]])
	except Exception as e:
		print('range_assigner')
		print(e)
		return input



##### Cached Corrections
def Cache_Lookup(Replace_String, Input_String, Cache_Dict_String):
	if Replace_String is None:
		return Replace_String
	try:
		Cache_Dict = eval(Cache_Dict_String)
		if len(Cache_Dict) == 0:
			return Replace_String
		elif ((len(Cache_Dict) == 1) & ((Cache_Dict.keys() == ['nan']) | (Cache_Dict.keys() == [' ']) )):
			return Replace_String
		else:
			return "%A%"+Cache_Dict[Input_String][0]
	except Exception as e:
		return Replace_String


def is_abbrevation(abbrev, text):
	try:
		abbrev=abbrev.lower()
		text=text.lower()
		words=text.split()
		if not abbrev:
			return True
		if abbrev and not text:
			return False
		if abbrev[0]!=text[0]:
			return False
		else:
			return (is_abbrevation(abbrev[1:],' '.join(words[1:])) or
					any(is_abbrevation(abbrev[1:],text[i+1:])
						for i in range(len(words[0]))))
	except Exception as e:
		print('is abbrev')
		print(e)
		return abbrev

def Field_Resolver_Df_Based(Field_Original, Field_Replace,Field_Flag, DictStr):
	if Field_Flag =='A':
		return(Field_Replace)
	if Field_Replace is None:
		return(Field_Replace)
	if Field_Replace.isdigit():
		return(''.join(["%X%",str(Field_Replace)]))
		#return ("%X%"+str(Field_Replace))
	try:
		DD_String = (' '.join(eval(DictStr).keys()))
		tokens_IncumbentData = Field_Replace.encode('utf-8').split(' ')
		tokens_Ref_Data1 = DD_String.encode('utf-8').split(' ')
		m = 0
		for i in tokens_IncumbentData:
			r1 = filter(lambda x, y = i : is_abbrevation(y, x) ,tokens_Ref_Data1) 
			if len(r1) == 0:
				tokens_IncumbentData[m] = tokens_IncumbentData[m]
			else:
				tokens_IncumbentData[m] = filter(lambda x : len(x) == min(map(lambda x: len(x), r1)) ,r1 )[0]
			m = m +1
		Field_Replace = ' '.join(tokens_IncumbentData)
		Dict = eval(DictStr)
		Threshold1 = 40
		Threshold2 = 95
		Tempitem = [('DD_actuals',Dict.keys())]
		Tempdf = pandas.DataFrame.from_items(Tempitem)
		Tempdf['field_replace'] = Field_Replace 
		Tempdf['dist'] = Tempdf.apply(lambda x: fuzz.token_set_ratio(x.DD_actuals.lower(), x.field_replace.lower()) , axis = 1)
		if Tempdf.dist.max() <= Threshold1:
			return(''.join(["%X%",str(Field_Replace)]))
		elif Tempdf.dist.max() <= Threshold2:
			return (''.join(["%N1%",str(Tempdf.iloc[Tempdf.dist.idxmax()]['DD_actuals'])]))
		else:
			return (''.join(["%N%",str(Tempdf.iloc[Tempdf.dist.idxmax()]['DD_actuals'])]))
	except Exception as e:
		print('Field_Resolver')
		print(e)
		return(''.join(["%X%",str(Field_Replace)]))
		#return ("%X%"+str(Field_Replace))
		

def Field_Resolver_Dict_Based(Field_Original, Field_Replace,Field_Flag, DictStr):
	if Field_Flag =='A':
		return(Field_Replace)
	if Field_Replace is None:
		return(Field_Replace)
	if Field_Replace.isdigit():
		return(''.join(["%X%",str(Field_Replace)]))
		#return ("%X%"+str(Field_Replace))
	try:
		DD_String = (' '.join(eval(DictStr).keys()))
		tokens_IncumbentData = Field_Replace.encode('utf-8').split(' ')
		tokens_Ref_Data1 = DD_String.encode('utf-8').split(' ')
		m = 0
		for i in tokens_IncumbentData:
			r1 = filter(lambda x, y = i : is_abbrevation(y, x) ,tokens_Ref_Data1) 
			if len(r1) == 0:
				tokens_IncumbentData[m] = tokens_IncumbentData[m]
			else:
				tokens_IncumbentData[m] = filter(lambda x : len(x) == min(map(lambda x: len(x), r1)) ,r1 )[0]
			m = m +1
		Field_Replace = ' '.join(tokens_IncumbentData)
		Dict = eval(DictStr)
		Threshold1 = 40
		Threshold2 = 95
		DD_Value = Dict.keys()
		
		DD_Dist = map(lambda x,y = Field_Replace: fuzz.token_sort_ratio(y.lower(), x.lower()), DD_Value)
		DD_Dict = dict(zip(DD_Dist, DD_Value))
		
		if max(DD_Dict.keys()) <= Threshold1:
			return(''.join(["%X%",str(Field_Replace)]))
		elif max(DD_Dict.keys()) <= Threshold2:
			return (''.join(["%N1%",str(DD_Dict.get(max(DD_Dict.keys())))]))
		else:
			return (''.join(["%N%", str(DD_Dict.get(max(DD_Dict.keys())))]))
		
	except Exception as e:
		print('Field_Resolver')
		print(e)
		return(''.join(["%X%",str(Field_Replace)]))
		#return ("%X%"+str(Field_Replace))
		
	


def Field_Resolver(Field_Original, Field_Replace,Field_Flag, DictStr):
	if Field_Flag =='A':
		return(Field_Replace)
	if Field_Replace is None:
		return(Field_Replace)
	if Field_Replace.isdigit():
		return(''.join(["%X%",str(Field_Replace)]))
		#return ("%X%"+str(Field_Replace))
	try:
		DD_String = (' '.join(eval(DictStr).keys()))
		tokens_IncumbentData = Field_Replace.encode('utf-8').split(' ')
		tokens_Ref_Data1 = DD_String.encode('utf-8').split(' ')
		m = 0
		for i in tokens_IncumbentData:
			r1 = filter(lambda x, y = i : is_abbrevation(y, x) ,tokens_Ref_Data1) 
			if len(r1) == 0:
				tokens_IncumbentData[m] = tokens_IncumbentData[m]
			else:
				tokens_IncumbentData[m] = filter(lambda x : len(x) == min(map(lambda x: len(x), r1)) ,r1 )[0]
			m = m +1
		Field_Replace = ' '.join(tokens_IncumbentData)
		r1 =[]
		r2 =[]
		Dict = eval(DictStr)
		Threshold1 = 40
		Threshold2 = 95
		r1 = filter(lambda x,y = Field_Replace: fuzz.token_set_ratio(y.lower(), x.lower()) >=  
		Threshold1, Dict.keys())
		if len(r1) == 0:
			return(''.join(["%X%",str(Field_Replace)]))
			#return ("%X%"+str(Field_Replace))
		r2 = filter(lambda x,y = Field_Replace: fuzz.token_sort_ratio(x.lower(), y.lower()) ==  
		max(map(lambda x,y = Field_Replace: fuzz.token_sort_ratio(x.lower(), y.lower()), r1)),r1)
		temp_Replace = str(Dict[r2[0]][0]) 
		if fuzz.token_set_ratio(temp_Replace.lower(), Field_Replace.lower()) >= Threshold2:
			return(''.join(["%N%",temp_Replace]))
			#return("%N%"+temp_Replace)
		else:
			return(''.join(["%N1%",temp_Replace]))
			#return("%N1%"+temp_Replace)
	except Exception as e:
		print('Field_Resolver')
		print(e)
		return(''.join(["%X%",str(Field_Replace)]))
		#return ("%X%"+str(Field_Replace))
		

def Field_Flag_Chopper(Replace_String):
	if Replace_String is None:
		return Replace_String
	try:
		if ((Replace_String.startswith("%A%")) | (Replace_String.startswith("%N%"))) :
			return Replace_String[3:]
		elif (Replace_String.startswith("%N1%")):
			return Replace_String[4:]
		else:
			return Replace_String
	except Exception as e:
		print(e)
		return Replace_String


def Layer_Flag_Update_V1(Replace_String):
	if Replace_String is None:
		return "X"
	try:
		if Replace_String.startswith("%A%"):
			return "A"
		elif Replace_String.startswith("%N1%"):
			return "N1"
		elif Replace_String.startswith("%N%"):
			return "N"
		else:
			return "X"
	except Exception as e:
		return "X"
		


if __name__ == '__main__':
	
	try:
		
		import MySQLdb
		import numpy as np
		from multiprocessing import Pool
		import sys
		import json
		import pandas
		import xlrd
		import re
		from dateutil import parser
		from datetime import datetime, timedelta
		from pandas.io.json import json_normalize
		pandas.options.mode.chained_assignment = None
		from pandas.io import sql
		from sqlalchemy import create_engine
		import string
		from fuzzywuzzy import fuzz
		a = datetime.now()
		input1 = '/home/maxiq/Autocorrect/Data/testing/demo_campaign_emp_data_meta.json'
		input2 = '/home/maxiq/Autocorrect/Data/testing/Raw_Data_Subset_1.csv'
		JsonString = input1
		DataString = input2
		#JsonString = sys.argv[1]
		#DataString = sys.argv[2]
	
		MData = pandas.read_json(JsonString)
		MJSON_String = MData.to_json(orient = 'records')
		MJson_Raw = json.loads(MJSON_String) 
		ContextData = pandas.DataFrame(columns=['entityid','campaignId', 'companyName', 'cpyCode', 'ctryCode', 'grpCode', 
														'industry.sector', 'industry.subSector','industry.superSector', 
														'orgSize', 'sectionId','uniqueIdColumnCode' ])
		
		QuestionMaster = pandas.DataFrame(columns=['entityid','code', 'dataType', 'displayLabel', 'questionType'])
		Output_Directory_String = '/home/maxiq/Mercer_ML_Directory/Autocorrect/Data/Outgoing/Data/'
		Output_File_String = DataString.split('/')[-1].split('.')[0]+'.'+str(a.strftime('%a%d%b%Y%H:%M:%S'))+'.'+DataString.split('/')[-1].split('.')[-1].lower()
		#Output_File_String = DataString.split('/')[-1]
		
		Incoming_Data = pandas.read_csv(DataString,sep=',').astype(str)
		Incoming_Data1 = Incoming_Data
		Entitylist = {'Entityid': 'Entityname'}
		#ContextData extract block 
		for i in range(0,len(MJson_Raw)):
			ContextData2 = json_normalize(MJson_Raw[i]['entities'])
			ContextData2 = ContextData2[filter(lambda x :  x.find('contextData') != -1, ContextData2.columns)]
			ContextData2.columns = map(lambda x: x.replace('contextData.', ''), ContextData2.columns)
			ContextData2['entityid'] = i
			ContextData = ContextData.append(ContextData2)
			
		for i in range(0,len(MJson_Raw)):
			Entitylist[i] = ContextData.iloc[i].fillna('').companyName +'_'+ContextData.iloc[i].fillna('').ctryCode
		
		del Entitylist['Entityid']
		ContextData = ContextData.replace({"entityid": Entitylist})
		#Section Structure Extract block
		for i in range(0,len(MJson_Raw)):
			SectionStructure = json_normalize(MJson_Raw[i]['entities'])['sectionStructure.columns']
			SectionStructureList = (SectionStructure.tolist())
			SectionStructure2 = json_normalize(SectionStructureList[0])
			QuestionMasterTemp = SectionStructure2[['code','dataType','displayLabel','questionType']]
			QuestionMasterTemp['entityid'] = i
			QuestionMaster = QuestionMaster.append(QuestionMasterTemp)
		QuestionMaster = QuestionMaster.replace({"entityid": Entitylist})
		QuestionMaster_list = QuestionMaster.code.unique().tolist()
		Incoming_Data_Header = Incoming_Data.columns.tolist()
		#if cmp(QuestionMaster_list, Incoming_Data_Header) != 0:
		#    raise Exception
		
		#def Autocorrection_Wrapper(QuestionMaster,ContextData,Incoming_Data1,SectionStructure2):
		## Generate the flags for Metadata Categorization
		DropDown1 = SectionStructure2[SectionStructure2.questionType == 'dropdown'][['code','displayLabel','options','dataType','questionType']]
		DropDown1['DD_Status'] = DropDown1.apply(lambda x: DD_Status_Flag(x['options']), axis= 1)
		DropDownMaster = DD_Label_Value(DropDown1)
		#DropDownMaster.to_csv('/home/maxiq/Autocorrect/Data/json/DropDownMaster16oct.csv', sep=',', encoding='utf-8', index  = False)
		QuestionMaster['Flag1'] = QuestionMaster.apply(lambda x: Flag_1(x['code'],x['dataType'],x['questionType'], DropDownMaster),axis=1)  
		QuestionMaster['Flag2'] = QuestionMaster.apply(lambda x: Flag_2(x['code'],x['dataType'],x['questionType'],x['Flag1'], DropDownMaster),axis=1)
		
		
		## Bucket the incoming columns for grouped processing
		Free_Text_Columns = sorted(QuestionMaster[QuestionMaster.questionType == 'text']['code'].unique().tolist())
		Double_Columns = sorted(QuestionMaster[QuestionMaster.questionType == 'double']['code'].unique().tolist())
		Percentage_Columns = sorted(QuestionMaster[QuestionMaster.questionType == 'percentage']['code'].unique().tolist())
		Integer_Columns = sorted(QuestionMaster[QuestionMaster.questionType == 'integer']['code'].unique().tolist())
		Date_Columns = sorted(QuestionMaster[QuestionMaster.questionType == 'date']['code'].unique().tolist())
		DD_Others_Columns = sorted(QuestionMaster[QuestionMaster.Flag2 == 'DD_Others']['code'].unique().tolist())
		DD_Range_Columns = sorted(QuestionMaster[QuestionMaster.Flag2 == 'DD_Range']['code'].unique().tolist())
		DD_Yes_No_Columns = sorted(QuestionMaster[QuestionMaster.Flag2 == 'DD_Yes_No']['code'].unique().tolist())
		DD_Country_Columns = sorted(QuestionMaster[QuestionMaster.Flag2 == 'DD_Country']['code'].unique().tolist())
		DD_Currency_Columns = sorted(QuestionMaster[QuestionMaster.Flag2 == 'DD_Currency']['code'].unique().tolist())
		DD_No_Act_Columns = sorted(QuestionMaster[(QuestionMaster.questionType == 'dropdown') & (QuestionMaster.Flag2 == 'False')  ]['code'].unique().tolist())
	
		Free_Text_Columns = filter(lambda x: x.startswith(tuple(Incoming_Data.columns)), Free_Text_Columns)
		Double_Columns = filter(lambda x: x.startswith(tuple(Incoming_Data.columns)), Double_Columns)
		Percentage_Columns = filter(lambda x: x.startswith(tuple(Incoming_Data.columns)), Percentage_Columns)
		#print(Percentage_Columns)
		Integer_Columns = filter(lambda x: x.startswith(tuple(Incoming_Data.columns)), Integer_Columns)
		Date_Columns = filter(lambda x: x.startswith(tuple(Incoming_Data.columns)), Date_Columns)
		DD_Others_Columns = filter(lambda x: x.startswith(tuple(Incoming_Data.columns)), DD_Others_Columns)
		DD_Range_Columns = filter(lambda x: x.startswith(tuple(Incoming_Data.columns)), DD_Range_Columns)
		DD_Yes_No_Columns = filter(lambda x: x.startswith(tuple(Incoming_Data.columns)), DD_Yes_No_Columns)
		DD_Country_Columns = filter(lambda x: x.startswith(tuple(Incoming_Data.columns)), DD_Country_Columns)
		DD_Currency_Columns = filter(lambda x: x.startswith(tuple(Incoming_Data.columns)), DD_Currency_Columns)
		DD_No_Act_Columns = filter(lambda x: x.startswith(tuple(Incoming_Data.columns)), DD_No_Act_Columns)
		
		print("Column Categorization")
		
		#Incoming_Data = pandas.read_csv('/home/maxiq/Autocorrect/Data/incoming/Raw_Data.csv' , sep='\t').astype(str)
		Incoming_Cols = Incoming_Data.columns
		for col in Incoming_Cols:
			Incoming_Data[''.join([col,"_Replace"])] = Incoming_Data[col]
			Incoming_Data[''.join([col,"_ACflag"])] = ""
					
						
		
		print("DD Corrections")
		def DD_MP(Incoming_Data2):
			
			for col in Incoming_Cols:
				Incoming_Data2[''.join([col,"_Replace"])] = Incoming_Data2[col]
				Incoming_Data2[''.join([col,"_ACflag"])] = ""
					
		
		
		#print("Spaces Stripper")
			for col in Incoming_Cols:
				Incoming_Data2[''.join([col,"_Replace"])] = Incoming_Data2[col].str.strip()
		
		#print("Garbage Corrections")
			for col in Incoming_Cols:   
				Incoming_Data2[''.join([col,"_Replace"])] = Incoming_Data2.apply(lambda x: Garbage_Corrections(x[''.join([col,"_Replace"])]), axis = 1)
		
		#print("Comma Remover")
			for col in DD_Range_Columns + Integer_Columns + Date_Columns:
				Incoming_Data2[''.join([col,"_Replace"])] = Incoming_Data2.apply(lambda x: remove_comma(x[''.join([col,"_Replace"])]), axis = 1)
		
		#print("Comma percentage Double")
			for col in Double_Columns + Percentage_Columns:
		    #print('in Comma remover')
				Incoming_Data2[''.join([col,"_Replace"])] = Incoming_Data2.apply(lambda x: remove_comma_PercentageDouble(x[''.join([col,"_Replace"])]), axis = 1)    
		
		#print("flagtext2int4")
			for col in Percentage_Columns + DD_Range_Columns + Double_Columns + Integer_Columns:
				Incoming_Data2[''.join([col,"_Replace"])] = Incoming_Data2.apply(lambda x: text2int(x[''.join([col,"_Replace"])]), axis = 1)
		
		
			#print("Percentage formatter")
		
			for col in Percentage_Columns:
		    #print(col)
				Incoming_Data2[''.join([col,"_Replace"])] = Incoming_Data2.apply(lambda x: Percent_To_DoubleFormatter(x[''.join([col,"_Replace"])]), axis = 1)
		
			#print("currency 2 num")
			for col in DD_Range_Columns + Double_Columns + Integer_Columns:
				Incoming_Data2[''.join([col,"_Replace"])] = Incoming_Data2.apply(lambda x: currency2num(x[''.join([col,"_Replace"])]), axis = 1)
		
			#print("Date Formatter")
			for col in Date_Columns:
				Incoming_Data2[''.join([col,"_Replace"])] = Incoming_Data2.apply(lambda x: DateFormater(x[''.join([col,"_Replace"])]), axis = 1)
		
			#print("Y N Binary")
			for col in DD_Yes_No_Columns:
				Incoming_Data2[''.join([col,"_Replace"])] = Incoming_Data2.apply(lambda x: Yes_No_binary(x[''.join([col,"_Replace"])]), axis = 1)
		
		
			#print("AC Corrections")
			db = MySQLdb.connect("hn1-mercer","root","maxiq","MercerACDB" )
			cursor = db.cursor()
			cursor.execute("SELECT * from Auto_Correction_Cache1")
			AC_DB = pandas.DataFrame(list(cursor.fetchall()), columns=map(lambda x:x[0], cursor.description))
			cursor.close()
			db.close()
			#AC_DB = pandas.read_csv('/home/maxiq/Autocorrect/Data/incoming/AutoCorrection_Cache.csv', sep = '|').astype(str)
			#AC_DB = pandas.read_csv('/home/maxiq/Autocorrect/Data/incoming/AC_DB_5.csv', sep = '|').astype(str)
		
			for col in Double_Columns + Percentage_Columns + Integer_Columns + Date_Columns + DD_Others_Columns + DD_Range_Columns + DD_Yes_No_Columns + DD_Country_Columns + DD_Currency_Columns + DD_No_Act_Columns:
				Cache_Dict_String = str(AC_DB[(AC_DB.Flag == 'A') & (AC_DB.Code == col)][['Match','Replace']].set_index('Match').T.to_dict('list'))
				Incoming_Data2[''.join([col,"_Replace"])] = Incoming_Data2.apply(lambda x: Cache_Lookup(x[''.join([col,"_Replace"])],x[col] ,Cache_Dict_String ), axis = 1)
				Incoming_Data2[''.join([col,"_ACflag"])] = Incoming_Data2.apply(lambda x: Layer_Flag_Update_V1(x[''.join([col,"_Replace"])]), axis = 1)
				Incoming_Data2[''.join([col,"_Replace"])] = Incoming_Data2.apply(lambda x: Field_Flag_Chopper(x[''.join([col,"_Replace"])]), axis = 1)
		
		
			for col in DD_Others_Columns + DD_Country_Columns + DD_Currency_Columns: # + DD_Yes_No_Columns:
				Ref_Data = DropDownMaster[DropDownMaster.code == col][['displayName','value']]
				Ref_Data1 = str(Ref_Data.set_index('displayName').T.to_dict('list'))
				Incoming_Data2[''.join([col,"_Replace"])] = Incoming_Data2.apply(lambda x: Field_Resolver_Dict_Based(x[col],x[''.join([col,"_Replace"])], x[col+'_ACflag'], Ref_Data1 ), axis = 1)
				Incoming_Data2[''.join([col,"_ACflag"])] = Incoming_Data2.apply(lambda x: Layer_Flag_Update_V1(x[''.join([col,"_Replace"])]), axis = 1)
				Incoming_Data2[''.join([col,"_Replace"])] = Incoming_Data2.apply(lambda x: Field_Flag_Chopper(x[''.join([col,"_Replace"])]), axis = 1)
			return Incoming_Data2
	
		
		num_partitions = 10 #number of partitions to split dataframe
		num_cores = 4 #number of cores on your machine
		df_split = np.array_split(Incoming_Data, num_partitions)	
		pool = Pool(processes=num_cores)
		Incoming_Data = pandas.concat(pool.map(DD_MP, df_split))
		pool.close()
		pool.join()	
		
		
		print("Range Formatter")
		## Range Assignment Columns Corrections
		#EMP_034_range = {
		#		250000:1, 
		#		500000:2,
		#		1000000:3,
		#		10000000000000000000000000000000000000000000:4
		#		
		#}
		#
		#EMP_396_range = {
		#		30000:36889, 
		#		50000:40205,
		#		75000:47648,
		#		100000:55914, 
		#		125000:64192,
		#		150000:72470,
		#		175000:80736, 
		#		200000:89014,
		#		250000:101419,
		#		300000:117963,
		#		350000:134507,
		#		400000:151051,
		#		500000:175873,
		#		600000:208961,
		#		700000:242061,
		#		800000:275149,
		#		80000000000000000000000000000000000000000:308237}
		#		
		#Incoming_Data['EMP_034_Replace'] = Incoming_Data.apply(lambda x: range_assigner(x['EMP_034_Replace'], str(EMP_034_range) ), axis = 1)
		
		#Incoming_Data['EMP_396_Replace'] = Incoming_Data.apply(lambda x: range_assigner(x['EMP_396_Replace'], str(EMP_396_range) ), axis = 1)
		## Layer Flag Update
		for col in Double_Columns + Percentage_Columns + Integer_Columns + Date_Columns + DD_Range_Columns + DD_No_Act_Columns :
			Incoming_Data[''.join([col,"_ACflag"])] = Incoming_Data.apply(lambda x: layer_Flag_update(x[''.join([col,"_Replace"])],x[col], x[''.join([col,"_ACflag"])] ), axis = 1)
		
		
		print("AC_DB update")
		db = MySQLdb.connect("hn1-mercer","root","maxiq","MercerACDB" )
		cursor = db.cursor()
		cursor.execute("SELECT * from Auto_Correction_Cache1")
		AC_DB = pandas.DataFrame(list(cursor.fetchall()), columns=map(lambda x:x[0], cursor.description))
		cursor.close()
		db.close()
		ProcessDf = AC_DB
		for col in Incoming_Cols:
			ProcessDf1 = Incoming_Data[Incoming_Data[''.join([col,"_ACflag"])] == 'N'][[col,col+'_Replace',col+'_ACflag']]
			ProcessDf1['Code'] = col
			ProcessDf1['Region'] = None
			ProcessDf1['Country'] = None
			ProcessDf1['DisplayLabel'] = col
			ProcessDf1.columns = ['Match', 'Replace', 'Flag', 'Code', 'Region', 'Country', 'DisplayLabel']
			ProcessDf = ProcessDf1.append(ProcessDf)
		
		ProcessDf = ProcessDf.drop_duplicates()
		
		
		engine = create_engine("mysql+mysqldb://root:maxiq@10.4.0.9:3306/MercerACDB",  echo = False)
		ProcessDf.to_sql(name = 'Auto_Correction_Cache1', con = engine, if_exists = 'replace', index = False)
		engine.dispose()
		#ProcessDf.to_csv('/home/maxiq/Autocorrect/Data/incoming/AC_DB_5.csv', sep='|', encoding='utf-8', index  = False)
		#Incoming_Data.to_csv('/home/maxiq/Autocorrect/Data/incoming/Roger_That.csv', sep='|', encoding='utf-8', index  = False)
				
		Df = Incoming_Data
		#print(Df)
		b = datetime.now()
		#print(Df.head())
		Df.to_csv(Output_Directory_String+Output_File_String, sep=',', encoding='utf-8', index  = False)
		print(Df.head())
		print(b-a)
		print('ML SUCCEED')
	except Exception as e:
		Incoming_Cols = Incoming_Data1.columns
		for col in Incoming_Cols:
			Incoming_Data1[''.join([col,"_Replace"])] = Incoming_Data1[col]
			Incoming_Data1[''.join([col,"_ACflag"])] = 'X'
		print(e)
		print ('ML FAILED')

		
