grammar Expression;

options
{
	language = Java;
}

@header {
package com.lti.mosaic.antlr4.parser;
}

/*
 *  Parser Grammar Starts here 
 */
parse
 : block EOF
 ;

block
 : statements*
 ;

statements
 : assignment  #assignmentExpr
 | ifStat     #ifStatExpr
 | whileStat  #whileStatExpr
 | statement   #statementExpr
 | OTHER {System.err.println("unknown char: " + $OTHER.text);} #otherExpr
 ;

assignment
 : ID ASSIGN expr SCOL
 ;

ifStat
 : MOSAIC_IF conditionBlock (MOSAIC_ELSE MOSAIC_IF conditionBlock)* (MOSAIC_ELSE statBlock)?
 ;

conditionBlock
 : expr statBlock
 ;

statBlock
 : OBRACE block CBRACE
 | statements
 ;

whileStat
 : WHILE expr statBlock
 ;

statement
 : expr SCOL
 ;

expr
 : 
 functions							#functionExpr
 | <assoc=left> expr POW expr			#powExpr
 | MINUS expr                           #unaryMinusExpr
 | NOT expr                             #notExpr
 | expr op=(MULT | DIV | MOD) expr      #multiplicationExpr
 | expr op=(PLUS | MINUS) expr          #additiveExpr
 | expr op=(LTEQ | GTEQ | LT | GT) expr #relationalExpr
 | expr op=(EQ | NEQ) expr              #equalityExpr
 | expr MOSAIC_AND expr                 #andExpr
 | expr MOSAIC_OR expr                  #orExpr
 | atom                                 #atomExpr
 ;

//================================================================================

//Functions Grouping

functions
:	  functionParam0   # zeroParamterFunctions
	| functionParam1   # singleParamterFunctions
	| functionParam2   # twoParamterFunctions
	| functionParam3   # threeParamterFunctions
	| functionParam4   # fourParamterFunctions
	| functionParamN  # NParametersFunctions
;

//Function Definitions
functionParam0
 : FUNCTIONNAME_PARAM0 LPAREN RPAREN
 ;

functionParam1
 : FUNCTIONNAME_PARAM1 LPAREN arguments1 RPAREN
 ;
 
functionParam2
 : FUNCTIONNAME_PARAM2 LPAREN arguments2 RPAREN
 ;
 
functionParam3
 : FUNCTIONNAME_PARAM3 LPAREN arguments3 RPAREN
 ;
 
functionParam4
 : FUNCTIONNAME_PARAM4 LPAREN arguments4 RPAREN
 ;
 
functionParamN
 : FUNCTIONNAME_PARAM_N LPAREN argumentsN RPAREN
 ; 
 
//Arguments 
argumentsN
 : expr? ( ',' expr? )*
 ;
 
arguments1
 : param1=expr?
 ;

arguments2
 : param1=expr? COMMA param2=expr?
 ;
 
/*
 * function_string
 : functions | STRING
 ; 
 */
 
arguments3		
 : param1=expr? COMMA param2=expr? COMMA param3=expr? 
 ;
 
arguments4
 : param1=expr? COMMA param2=expr? COMMA param3=expr? COMMA param4=expr? 
 ;

atom
 : LPAREN expr RPAREN   		#parExpr
 | value_list					#arrayAtom
 | (INT | DOUBLE)  				#numberAtom
 | (MOSAIC_TRUE | MOSAIC_FALSE) #booleanAtom
 | ID             				#idAtom
 | STRING         				#stringAtom
 | NIL            				#nilAtom
 ;
 
value_list
 : BEGL array? ENDL
 ;

array
 : arrayElement ? ( COMMA arrayElement ?)* # arrayValues
 ;


/*
 *  Parser Grammar Ends Here 
 */

/*
 *  Lexer Grammar Starts Here 
 */
 
//================================================================================
//Functions
FUNCTIONNAME_PARAM0 : 'RAND' | 'NOW';

FUNCTIONNAME_PARAM1 : 'LENGTH' | 'LOWER' | 'UPPER' | 'REVERSE' | 'TRIM' |'LTRIM' | 'RTRIM' |
					  'INITCAP' | 'SOUNDEX' | 'ISBLANK' | 'ISNOTBLANK' | 'SPACE' |
					  'COS' | 'TAN' | 'SIN' | 'ACOS' | 'ATAN' | 'ASIN' | 'LOG' | 'LOG2' | 'LOG10' |
					  'SQRT' | 'LN' | 'ABS' | 'FACT' | 'RAND' | 'FLOOR' | 'CEIL' | 'YEAR' |'INVERSIONCHECK' | 'EXP' 
					  | 'TO_STRING' | 'TO_NUMBER';	  
			  
FUNCTIONNAME_PARAM2 : 'CONCAT' | 'REPEAT' | 'INSTR' | 'LEVENSHTEIN' | 'REGEX_MATCH' | 'ROUND' | 'ROUNDP' | 
					  'TO_DATE' | 'ADD_MONTHS' | 'DATEDIFF' | 'BOOL_IN_LOOKUP'| 'NVL' | 'FIND_IN_SET' | 'SPLIT' ;

FUNCTIONNAME_PARAM3 : 'DECODE' | 'LOCATE' | 'RPAD' | 'LPAD' | 'SUBSTR' | 'SUBSTRP' |
					  'REGEXP_REPLACE' | 'TRANSLATE' | 'REGEXP_EXTRACT' | 'MID' | 'LOOK_UP_CHECK' |
					  'XAVG'| 'XCOUNT'| 'XMAX'|'XMIN'|'XSUM'|'XUNIQUECOUNT'|'AGGREGATERANGECHECK' ;

FUNCTIONNAME_PARAM4 : 'LOOKUPMAP' | 'LOOKUP' |'XFREQCOUNT'|'XFREQPERC'|'XPERC';

FUNCTIONNAME_PARAM_N : 'GREATEST' | 'LEAST' ;
//================================================================================

arrayElement
:
	atom # arrayElementTypes
;

MOSAIC_OR : '||' | 'OR' | 'or';

MOSAIC_AND : '&&' | 'AND' | 'and';

EQ : '==';

NEQ : '!=';

GT : '>';

LT : '<';

GTEQ : '>=';

LTEQ : '<=';

PLUS : '+';

MINUS : '-';

MULT : '*';

DIV : '/';

MOD : '%';

POW : '^';

NOT : '!';

SCOL : ';';

COMMA : ',';

ASSIGN : '=';

LPAREN : '(';

RPAREN : ')';

OBRACE : '{';

CBRACE : '}';

BEGL : '[';

ENDL : ']';

MOSAIC_TRUE : 'TRUE' | 'True' | 'true';

MOSAIC_FALSE : 'FALSE' | 'False' | 'false';

NIL : 'null';

MOSAIC_IF :   'IF' | 'if';

MOSAIC_ELSE : 'ELSE' | 'else' ;

WHILE : 'while';

ID  : [a-zA-Z_] [a-zA-Z_0-9]* ;

INT  : [0-9]+  ;

DOUBLE  : [0-9]+ '.' [0-9]*  | '.' [0-9]+ ;

STRING  : '"' (~["\r\n] | '\\"')* '"' ;

COMMENT  : '#' ~[\r\n]* -> skip  ;

SPACE  : [ \t\r\n] -> skip  ;

OTHER  : .  ;
