package com.lti.mosaic.db;

import java.sql.Connection;  
import java.sql.DriverManager;  
import java.sql.PreparedStatement;  
import java.sql.ResultSet;  
import java.sql.SQLException;
import java.sql.Statement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.lti.mosaic.cache.Cache;
import com.lti.mosaic.parser.constants.CacheConstants;


/**
 * use DerbyConnection.java instead
 * @author Nikhil Kshirsagar
 * @since 11-10-2017
 * @version 1.0 
 */
public class MysqlConnectionMercer {

	private static final String MYSQL_GET = " : >> get()";

	private static final Logger logger = LoggerFactory.getLogger(MysqlConnectionMercer.class);

	// Create a JDBCSingleton class.  
	// Static member holds only one instance of the JDBCSingleton class.         
	private static MysqlConnectionMercer jdbc;  
	static Connection con;
	private static final String FORMAT_CONSTANT = "{} {}";
 	
	public static MysqlConnectionMercer getJdbc() {
		return jdbc;
	}

	public static void setJdbc(MysqlConnectionMercer jdbc) {
		MysqlConnectionMercer.jdbc = jdbc;
	}

	public static Connection getCon() {
		return con;
	}

	public static void setCon(Connection con) {
		MysqlConnectionMercer.con = con;
	}

	// JDBCSingleton prevents the instantiation from any other class.  
	private MysqlConnectionMercer() {  }  

	// Providing global point of access.  
	public static MysqlConnectionMercer getInstance() {    
		logger.info("{} : >> getInstance()",MysqlConnectionMercer.class.getClass().getName());
		if (jdbc==null) {  
			jdbc=new  MysqlConnectionMercer();  
		}  

		logger.info("{} : << getInstance()",MysqlConnectionMercer.class.getClass().getName());
		return jdbc;  
	}
	
	// Get connection from methods like insert, view etc.   
	public static Connection getConnection() throws ClassNotFoundException, SQLException {
		logger.info("{} : >> getConnection()",MysqlConnectionMercer.class.getClass().getName());		  
		if(null == con) {
		  
		  try {
		    logger.info("MERCER_DB_DRIVER :: " + Cache.getProperty(CacheConstants.MERCER_DB_DRIVER)
		    + "\n MERCER_DB_ADDRESS/MERCER_DB_NAME ::" + Cache.getProperty(CacheConstants.MERCER_DB_ADDRESS)
		    + Cache.getProperty(CacheConstants.MERCER_DB_NAME) + "\n MERCER_DB_USERNAME ::"
		    + Cache.getProperty(CacheConstants.MERCER_DB_USERNAME));
		    
		    Class.forName(Cache.getProperty(CacheConstants.MERCER_DB_DRIVER));
		    
		    con = DriverManager.getConnection(
		        Cache.getProperty(CacheConstants.MERCER_DB_ADDRESS)
		        + Cache.getProperty(CacheConstants.MERCER_DB_NAME),
		        Cache.getProperty(CacheConstants.MERCER_DB_USERNAME),
		        Cache.getProperty(CacheConstants.MERCER_DB_PRIVATEKEY));
		  } catch (Exception e) {
		    logger.info("Error occurred while MySql connection :: {}", e.getMessage());
		    throw e;
		  }
		}

		logger.info("{} : << getConnection()",MysqlConnectionMercer.class.getClass().getName());
		return con;
	}  

	
	// Insert the record into the database   
	public int insert(String query, String[] args) throws SQLException {  
		logger.info("{} : >> insert()",MysqlConnectionMercer.class.getClass().getName());
		
		Connection c=null;  
		PreparedStatement ps=null;  
		int recordCounter=0;  
		try {  
			c = MysqlConnectionMercer.getConnection();  

			ps=c.prepareStatement(query);  
			for(int i = 0; i<args.length; i++){
				ps.setObject(i+1, args[i]);
			}
			
			recordCounter=ps.executeUpdate();  
		
		} catch (Exception e) { 
			logger.info(e.getMessage());
		} finally{  
			if (ps!=null){  
				ps.close();  
			}
			if(c!=null){  
				c.close();  
			}   
		}  
		logger.info("{} : << insert()",MysqlConnectionMercer.class.getClass().getName());
		return recordCounter;  
	}  

	//to view the data from the database     
	// For PreparedStatement use
	public ResultSet get(Connection conn, String query, String[] args) throws SQLException {
		logger.info(FORMAT_CONSTANT,MysqlConnectionMercer.class.getClass().getName(), MYSQL_GET);
		ResultSet rs = null;
		try(PreparedStatement pStmt = conn.prepareStatement(query);) {
			if (args != null) {
				for (int i = 0; i < args.length; i++) {
					pStmt.setObject(i + 1, args[i]);
				}
			}
			rs = pStmt.executeQuery();
		} catch (Exception e) {
			logger.info(e.getMessage());
		}
		logger.info(FORMAT_CONSTANT,MysqlConnectionMercer.class.getClass().getName() , MYSQL_GET);
		return rs;
	}

	// For Statement use
	public ResultSet get(Connection conn, String query) throws SQLException {
		logger.info(FORMAT_CONSTANT,MysqlConnectionMercer.class.getClass().getName(), MYSQL_GET);
		ResultSet rs = null;
		try(Statement stmt = conn.createStatement();) {
			rs = stmt.executeQuery(query);
		} catch (SQLException e) {
			logger.debug(e.getMessage());
 			logger.info("{} : >> get() SQL Exception",MysqlConnectionMercer.class.getClass().getName());
		}
		logger.info(FORMAT_CONSTANT,MysqlConnectionMercer.class.getClass().getName(), MYSQL_GET);
		return rs;
	}

	// to update the password for the given username
	
	//Comment this Method - Not Being Used
	public int update(String name, String password) throws SQLException {
		logger.info("{} : >> update()",MysqlConnectionMercer.class.getClass().getName());
		Connection c = null;
		PreparedStatement ps = null;
	    StringBuilder query = new StringBuilder(" update userdata set upassword=? where uname='").append(name).append("' ");
 		int recordCounter = 0;
		try {
			c = MysqlConnectionMercer.getConnection();
			ps = c.prepareStatement(query.toString());
			ps.setString(1, password);
			recordCounter = ps.executeUpdate();
		} catch (Exception e) {
			logger.info(e.getMessage());
		} finally {

			if (ps != null) {
				ps.close();
			}
			if (c != null) {
				c.close();
			}
		}
		logger.info("{} : << update()",MysqlConnectionMercer.class.getClass().getName());
		return recordCounter;
	}
	
	//to delete the data from the database   
	public int delete(int userid) throws SQLException{  
		logger.info("{} : >> delete()",MysqlConnectionMercer.class.getClass().getName());
		Connection c=null;  
		PreparedStatement ps=null;  
		int recordCounter=0;  
		StringBuilder query = new StringBuilder(" delete from userdata where uid='").append(userid).append("' ");
		try {  
			c=MysqlConnectionMercer.getConnection();  
			ps=c.prepareStatement(query.toString());  
			recordCounter=ps.executeUpdate();  
		} catch (Exception e) { 
			logger.info(e.getMessage());
		}   
		finally{  
			if (ps!=null){  
				ps.close();  
			}
			if(c!=null){  
				c.close();  
			}   
		}  
		logger.info("{} : << delete()",MysqlConnectionMercer.class.getClass().getName());
		return recordCounter;  
	}

}
