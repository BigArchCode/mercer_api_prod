# Generated from Expression.g4 by ANTLR 4.7.1
from antlr4 import *

# This class defines a complete generic visitor for a parse tree produced by ExpressionParser.

class ExpressionVisitor(ParseTreeVisitor):

    # Visit a parse tree produced by ExpressionParser#parse.
    def visitParse(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by ExpressionParser#block.
    def visitBlock(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by ExpressionParser#assignmentExpr.
    def visitAssignmentExpr(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by ExpressionParser#ifStatExpr.
    def visitIfStatExpr(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by ExpressionParser#whileStatExpr.
    def visitWhileStatExpr(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by ExpressionParser#statementExpr.
    def visitStatementExpr(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by ExpressionParser#otherExpr.
    def visitOtherExpr(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by ExpressionParser#assignment.
    def visitAssignment(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by ExpressionParser#ifStat.
    def visitIfStat(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by ExpressionParser#conditionBlock.
    def visitConditionBlock(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by ExpressionParser#statBlock.
    def visitStatBlock(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by ExpressionParser#whileStat.
    def visitWhileStat(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by ExpressionParser#statement.
    def visitStatement(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by ExpressionParser#notExpr.
    def visitNotExpr(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by ExpressionParser#unaryMinusExpr.
    def visitUnaryMinusExpr(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by ExpressionParser#multiplicationExpr.
    def visitMultiplicationExpr(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by ExpressionParser#atomExpr.
    def visitAtomExpr(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by ExpressionParser#orExpr.
    def visitOrExpr(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by ExpressionParser#additiveExpr.
    def visitAdditiveExpr(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by ExpressionParser#powExpr.
    def visitPowExpr(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by ExpressionParser#relationalExpr.
    def visitRelationalExpr(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by ExpressionParser#equalityExpr.
    def visitEqualityExpr(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by ExpressionParser#functionExpr.
    def visitFunctionExpr(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by ExpressionParser#andExpr.
    def visitAndExpr(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by ExpressionParser#zeroParamterFunctions.
    def visitZeroParamterFunctions(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by ExpressionParser#singleParamterFunctions.
    def visitSingleParamterFunctions(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by ExpressionParser#twoParamterFunctions.
    def visitTwoParamterFunctions(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by ExpressionParser#threeParamterFunctions.
    def visitThreeParamterFunctions(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by ExpressionParser#fourParamterFunctions.
    def visitFourParamterFunctions(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by ExpressionParser#NParametersFunctions.
    def visitNParametersFunctions(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by ExpressionParser#functionParam0.
    def visitFunctionParam0(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by ExpressionParser#functionParam1.
    def visitFunctionParam1(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by ExpressionParser#functionParam2.
    def visitFunctionParam2(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by ExpressionParser#functionParam3.
    def visitFunctionParam3(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by ExpressionParser#functionParam4.
    def visitFunctionParam4(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by ExpressionParser#functionParamN.
    def visitFunctionParamN(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by ExpressionParser#argumentsN.
    def visitArgumentsN(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by ExpressionParser#arguments1.
    def visitArguments1(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by ExpressionParser#arguments2.
    def visitArguments2(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by ExpressionParser#arguments3.
    def visitArguments3(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by ExpressionParser#arguments4.
    def visitArguments4(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by ExpressionParser#parExpr.
    def visitParExpr(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by ExpressionParser#arrayAtom.
    def visitArrayAtom(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by ExpressionParser#numberAtom.
    def visitNumberAtom(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by ExpressionParser#booleanAtom.
    def visitBooleanAtom(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by ExpressionParser#idAtom.
    def visitIdAtom(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by ExpressionParser#stringAtom.
    def visitStringAtom(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by ExpressionParser#nilAtom.
    def visitNilAtom(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by ExpressionParser#value_list.
    def visitValue_list(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by ExpressionParser#arrayValues.
    def visitArrayValues(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by ExpressionParser#arrayElementTypes.
    def visitArrayElementTypes(self, ctx):
        return self.visitChildren(ctx)


