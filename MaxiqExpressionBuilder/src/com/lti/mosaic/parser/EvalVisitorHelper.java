package com.lti.mosaic.parser;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.lti.mosaic.antlr4.parser.ExpressionParser;
import com.lti.mosaic.function.def.Value;


/**
 * 
 * @author Sarang Gandhi
 * @version 0.1
 * 
 *          This helper class contains true data type calculation of Input and
 *          output values.
 * 
 */
public class EvalVisitorHelper {

	private static final Logger logger = LoggerFactory.getLogger(EvalVisitorHelper.class);
	
	public static final String DATE_FORMAT = "MM/dd/yyyy";

	private EvalVisitorHelper() {
	}

	/**
	 * 
	 * @param left
	 *            Value object
	 * @param right
	 *            Value object
	 * @param operator
	 *            Binary Operator
	 * @param local
	 *            Lang Locale
	 * @return Value Obeject
	 * 
	 *         Following method supports {+,-,*,/,%,^,==,!=,>,>=,<,<=} these 12
	 *         operations for all data types and returns true data type.
	 */
	public static Value calculateBinaryOperations(Value left, Value right, int operator, Locale local) {
		//Locale will be used further improvement.
		logger.info(" calculateBinaryOperations >> Locale : {}",local);
		try{
		switch (operator) {

		case ExpressionParser.PLUS:
			return new Value(left.asBigDecimal().add(right.asBigDecimal()));

		case ExpressionParser.MINUS:
			return new Value(left.asBigDecimal().subtract(right.asBigDecimal()));
			
		case ExpressionParser.MULT:
			return new Value(left.asBigDecimal().multiply(right.asBigDecimal()));
		case ExpressionParser.DIV:
			return new Value(left.asBigDecimal().divide(right.asBigDecimal(),8,RoundingMode.HALF_UP));

		case ExpressionParser.MOD:
			return new Value(left.asBigDecimal().remainder(right.asBigDecimal()));

		case ExpressionParser.POW:
			return new Value(left.asBigDecimal().pow(right.asInteger()));

		case ExpressionParser.EQ:
			return new Value(calculateEqual(left, right));

		case ExpressionParser.NEQ:
			return new Value(!calculateEqual(left, right));

		case ExpressionParser.LT:
			return lessThanCompare(left, right);
			 
		case ExpressionParser.LTEQ:
			return lessThanEqualToComapare(left, right);
			
		case ExpressionParser.GT:
			return greaterThanCompare(left, right);
			
		case ExpressionParser.GTEQ:
			return greaterThanEqualToCompare(left, right);
			
		default:
			throw new RuntimeException("unknown operator: " + ExpressionParser.tokenNames[operator]);
		}
		}
		catch(Exception e){
			return null;
		}
	}

	/**
	 * @param left
	 * @param right
	 * @return
	 */
	private static Value greaterThanEqualToCompare(Value left, Value right) {
		if(left.isNumeric() && right.isNumeric()){
			return new Value(left.asBigDecimal().compareTo(right.asBigDecimal()) >= 0);
		}
		else if (left.isDate() && right.isDate()) {
			return null;
		}
		else
			return new Value(false);
	}

	/**
	 * @param left
	 * @param right
	 * @return
	 */
	private static Value greaterThanCompare(Value left, Value right) {
		if(left.isNumeric() && right.isNumeric()){
			return new Value(left.asBigDecimal().compareTo(right.asBigDecimal()) > 0);
		}
		else if (left.isDate() && right.isDate()) {
			return null;
		}
		else
			return new Value(false);
	}

	/**
	 * @param left
	 * @param right
	 * @return
	 */
	private static Value lessThanEqualToComapare(Value left, Value right) {
		if(left.isNumeric() && right.isNumeric()){
			return new Value(left.asBigDecimal().compareTo(right.asBigDecimal()) <= 0);
		}
		else if (left.isDate() && right.isDate()) {
			return null;
		}
		else
			return new Value(false);
	}

	/**
	 * @param left
	 * @param right
	 * @return
	 */
	private static Value lessThanCompare(Value left, Value right) {
		if(left.isNumeric() && right.isNumeric()){
			return new Value(left.asBigDecimal().compareTo(right.asBigDecimal()) < 0);
		}
		else if (left.isDate() && right.isDate()) {
			return null;
		}
		else
			return new Value(false);
	}

	/**
	 * @param left
	 * @param right
	 * @return
	 */
	private static boolean calculateEqual(Value left, Value right) {
		if(left.isNumeric() && right.isNumeric()){
			return left.asBigDecimal().compareTo(right.asBigDecimal()) == 0;
		}
		else if (left.isString() && right.isString()) {
			return left.asString().equals(right.asString());
		} 
		else if (left.isBoolean() && right.isBoolean()) {
			return left.asBoolean() == right.asBoolean();
		} else if (left.isDate() && right.isDate()) {
				return left.asDate().equals(right.asDate());
		}
		else 
			return false;
	}

	/**
	 * 
	 * @param Double
	 *            value
	 * @return Value object Double or Integer
	 */
	public static Value trueDataReturn(Double dbl) { // Converts calculation into true data type
														// E.g. 2.1 = 2.1 and 4.0 = 4
		Double primtD = dbl;

		if ((primtD - primtD.longValue()) != 0) {
			return new Value(dbl);
		} else {
			return new Value(dbl.longValue());
		}
	}

	public static int factorial(int n) {
		if (n == 0)
			return 1;
		else
			return (n * factorial(n - 1));
	}

	
	public static int log2(int x, int base)
	{
	    return (int) (Math.log(x) / Math.log(base));
	}
	
	
	public static double round(double value, int places) {
	    if (places < 0) throw new IllegalArgumentException();
	 
	    BigDecimal bd = new BigDecimal(Double.toString(value));
	    bd = bd.setScale(places, RoundingMode.HALF_UP);
	    return bd.doubleValue();
	}
}


