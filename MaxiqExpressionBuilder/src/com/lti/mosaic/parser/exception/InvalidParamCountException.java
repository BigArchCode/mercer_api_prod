package com.lti.mosaic.parser.exception;

/**
 * Licensed Information -- need to work on this 
 */

/**
 * Expception - In case count of param is mis match  
 * <p/>
 * 
 * @author Piyush
 * @since 1.0
 * @version 1.0
 */


public class InvalidParamCountException extends ExpressionEvaluatorException {
	
	private static final long serialVersionUID = 1L;

	public InvalidParamCountException(String message, Throwable throwable) {
		super(message, throwable);
	}

	public InvalidParamCountException(String message) {
		super(message);
	}
}
