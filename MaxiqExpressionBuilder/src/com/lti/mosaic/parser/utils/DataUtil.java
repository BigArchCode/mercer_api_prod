package com.lti.mosaic.parser.utils;

/**
 * 
 * @author Komal Kabra
 * @since 05-12-2017
 * @version 1.0
 * Class to sort the alphanumeric posCode into alphabetical order in case of alphabet and descending order in case of numbers
 */

import java.util.ArrayList;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.TreeSet;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/*
 * 
 * Add examples here 
 */
public class DataUtil implements Comparator<String>{
	
	private static final Logger logger = LoggerFactory.getLogger(DataUtil.class);
	/**
	 * 
	 * @param posCodeList
	 * @param familySet
	 * @return Sorted List.
	 */
	// Code to sort the alphanumeric posCode into alphabetical order in case of
	// alphabet and descending order in case of numbers.
	public static List<String> sortedList(List<String> posCodeList, TreeSet<String> familySet) {

		List<String> finalList = null;
		String family = null;
		String career = null;

		List<String> tempList = null;
		List<String> tempListCareer = null;
		List<String> careerSortedList = null;
		try {
			finalList = new LinkedList<>();
			tempList = new ArrayList<>();
			tempListCareer = new ArrayList<>();

			long startTimeForcreatingsublistOfFamily = System.currentTimeMillis();

			for (String familyName : familySet) {
				for (String singleRecord : posCodeList) {
					family = singleRecord.split(ExpressionBuilderConstants.REGEX_FILENAME_SPLIT)[0];
					career = singleRecord.split(ExpressionBuilderConstants.REGEX_FILENAME_SPLIT)[1];
					if (family.equalsIgnoreCase(familyName)) {
						tempListCareer.add(career);
					}
				}
				// Sort the list of career stream and career level list
				 careerSortedList = tempListCareer.parallelStream().sorted(new DataUtil())
						.collect(Collectors.toList());

				// Append the family to the career stream and career level list element.
				careerSortedList.replaceAll(s2 -> familyName + "." + s2);

				// Merge the list of each family in the final list.
				finalList.addAll(careerSortedList);
				tempListCareer.clear();
				logger.info("End time of FOR loop of adding sorted list into final list : {}"
					, (System.currentTimeMillis() - startTimeForcreatingsublistOfFamily));
			}
			
			logger.info("Sorted list : {}", careerSortedList);
		} finally {

			if (null != tempListCareer) {
				tempListCareer = null;
			}
			if (null != tempList) {
				tempList = null;
			}
			if (null != family) {
				family = null;
			}
			if (null != career) {
				career = null;
			}
		}

		return finalList;
	}

	/**
	 * 
	 * @param ch:
	 *            character from career stream and level.
	 * @return: true if character, false if not character.
	 */

	private final boolean isDigit(char ch) {
		return ((ch >= 48) && (ch <= 57));
	}

	/**
	 * Length of string is passed in for improved efficiency (only need to
	 * calculate it once)
	 **/
	/**
	 * 
	 * @param s:
	 *            career stream and level
	 * @param slength:
	 *            length career stream and level
	 * @param marker:
	 *            0
	 * @return: sorted string of career stream and career level
	 */
	private final String getChunk(String s, int slength, int marker) {
		StringBuilder chunk = new StringBuilder();
		char c = s.charAt(marker);
		chunk.append(c);
		marker++;
		if (isDigit(c)) {
			while (marker < slength) {
				c = s.charAt(marker);
				if (!isDigit(c))
					break;
				chunk.append(c);
				marker++;
			}
		} else {
			while (marker < slength) {
				c = s.charAt(marker);
				if (isDigit(c))
					break;
				chunk.append(c);
				marker++;
			}
		}
		return chunk.toString();
	}

	/**
	 * @param s1:
	 *            POS code from posCode list
	 * @param s2:
	 *            POS code from posCode list
	 * @return: 0,1,-1 Compares two POS code from POS Code list.
	 */
	public int compare(String s1, String s2) {
		if ((s1 == null) || (s2 == null)) {
			return 0;
		}
		
		int s1Length = s1.length();
		int s2Length = s2.length();
		
		int result = getComapreResult(s1,s2);
		if(result != Integer.MIN_VALUE) {
			return result;
		}
		return s1Length - s2Length;
	}

	private int getComapreResult(String s1, String s2) {

		int thisMarker = 0;
		int thatMarker = 0;
		int s1Length = s1.length();
		int s2Length = s2.length();
		
		while (thisMarker < s1Length && thatMarker < s2Length) {
			String thisChunk = getChunk(s1, s1Length, thisMarker);
			thisMarker += thisChunk.length();

			String thatChunk = getChunk(s2, s2Length, thatMarker);
			thatMarker += thatChunk.length();

			// If both chunks contain numeric characters, sort them numerically
			int result = sortAndComapreChunks(thisChunk,thatChunk);
			if (result != 0) {
				return result;
			}
		}
		//Returning Integer MIN value as a not Valid result 
		return Integer.MIN_VALUE;
	}

	private int sortAndComapreChunks(String thisChunk, String thatChunk) {
		int result = 0;
		if (isDigit(thisChunk.charAt(0)) && isDigit(thatChunk.charAt(0))) {
			// Simple chunk comparison by length.
			int thisChunkLength = thisChunk.length();
			result = thisChunkLength - thatChunk.length();
			// If equal, the first different number counts
			if (result == 0) {
				for (int i = 0; i < thisChunkLength; i++) {
					result = thatChunk.charAt(i) - thisChunk.charAt(i);
					if (result != 0) {
						return result;
					}
				}
			}
		} else {
			result = thisChunk.compareTo(thatChunk);
		}
		return result;
	}

}
