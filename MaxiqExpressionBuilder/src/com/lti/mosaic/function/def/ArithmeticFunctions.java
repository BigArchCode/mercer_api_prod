package com.lti.mosaic.function.def;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.lti.mosaic.parser.enums.FunctionEnum;

public class ArithmeticFunctions {
	
	private ArithmeticFunctions() {
	}

	public static Value getMinOrMax(List<Value> args, FunctionEnum functionName) {

		List<BigDecimal> bigDecimalList = new ArrayList<>();

		for (Value value : args) {

			if ((null == value) || (value.isNull())) {
				continue;
			}else if ( value.isInteger()) {
				bigDecimalList.add(BigDecimal.valueOf(Integer.valueOf(value.toString())));
			}else if(value.isDouble() ) {
				bigDecimalList.add(BigDecimal.valueOf(Double.valueOf(value.toString())));
			} else{
				return null;
			}
		}
		
		switch (functionName) {

		case GREATEST:
			return new Value(Collections.max(bigDecimalList));

		case LEAST:
			return new Value(Collections.min(bigDecimalList));

		default:
			break;

		}
		return null;
	}
}
