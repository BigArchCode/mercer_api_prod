package com.lti.mosaic.function.def;

import java.math.BigDecimal;
import java.util.List;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

 
public class Value {
	private static final Logger logger = LoggerFactory.getLogger(Value.class);

    public static final Value VOID = new Value(new Object());

    final Object currentValue;
    
    public Value(Object value) {
        this.currentValue = value;
    }

    public Boolean asBoolean() {
        return (Boolean)currentValue;
    }

    public BigDecimal asBigDecimal() {
		return (BigDecimal) currentValue;
	}
    
	public Double asDouble() {
		double val = ((BigDecimal) currentValue).doubleValue();
		logger.debug(" >> getFunction() : DOUBLE : original : {}  RETURNED : {}", currentValue, val);
		return val;
	}
	
	public Integer asInteger() {
		int val = ((BigDecimal) currentValue).intValue();
		logger.debug(" >> getFunction() : INTEGER : original : {}  RETURNED : {}", currentValue, val);
		return val;
	}

	@SuppressWarnings("unchecked")
	public List<Value> asArrayList() {
		return (List<Value>) currentValue;
	}
	
	@SuppressWarnings("unchecked")
	public Value[] asArray() {
		return (Value[]) currentValue;
	}
	
	public DateTime asDate() {
		logger.debug(" >> getFunction() : Date : original : {}  Date :  {}", currentValue , (DateTime) currentValue);
 		return (DateTime) currentValue;
	}
	
	public String asString() {
		 return (String) currentValue;
    }

	 public boolean isBoolean() {
		 return currentValue instanceof Boolean;
	 }
	
	 public boolean isString() {
		 return currentValue instanceof String;
	 }
	
	 public boolean isNumeric() {
	        return currentValue instanceof BigDecimal;
	 }
	 
	public boolean isInteger() {
		BigDecimal bdValue = (BigDecimal) currentValue;
		return bdValue.scale() <= 0 /*|| bdValue.signum() == 0 || bdValue.stripTrailingZeros().scale() <= 0*/;
	}
    
	public boolean isDouble() {
		return ((BigDecimal) currentValue).scale() > 0;
	}
    
    public boolean isDate() {
        return currentValue instanceof DateTime;
    }
    
    public boolean isArray() {
        return currentValue instanceof Value[];
    }
    
    public boolean isNull() {
        return this.currentValue == null;
    }
    
    
    @Override
    public int hashCode() {

        if(currentValue == null) {
            return 0;
        }

        return this.currentValue.hashCode();
    }

    @Override
    public boolean equals(Object o) {

        if(currentValue == o) {
            return true;
        }

        if(currentValue == null || o == null || o.getClass() != currentValue.getClass()) {
            return false;
        }

        Value that = (Value)o;

        return this.currentValue.equals(that.currentValue);
    }

    @Override
    public String toString() {
        return String.valueOf(currentValue);
    }
}