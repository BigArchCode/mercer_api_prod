package com.lti.mosaic.function.def;

import java.math.BigDecimal;
import java.util.Locale;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.chrono.ISOChronology;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class DateFunctions {

	private static final Logger logger = LoggerFactory.getLogger(DateFunctions.class);
	
	private static final String DATE_FUNCTIONS_YEAR_EXCEPTION = " << DateFunctions : perform() : year : Exception : ";
	private static final String DATE_FUNCTIONS_YEAR = "<< DateFunctions : perform() : year : ";
	private static final String FORMAT_CONSTANT = "{} {}";

	private DateFunctions() {
	}
	
	/**
	 * 
	 * @param date
	 * @param format
	 * @return dateTime as value
	 */
	public static Value toDate(String date, String format) {
		try {
			if (null != date) {
				// Default return FORMAT of to_date function : yyyy-MM-dd
				 
				DateTimeFormatter formatter = DateTimeFormat.forPattern(StringUtils.isEmpty(format) ? "yyyy-MM-dd" : format)
				        .withLocale(Locale.ROOT)
				        .withChronology(ISOChronology.getInstance()); 
				
				Value val = new Value(formatter.parseDateTime(date)); 
				logger.debug("<< DateFunctions : perform() : to_date : {}", val);
				logger.info("toDate : isdate - {}, to_date - {}", val.isDate(), val);
				
				return val;
			}
		} catch (Exception e) {
			logger.error(" << DateFunctions : perform() : to_date : Exception : {}", e.getMessage());
		}
		return null;
	}
	
	/**
	 * 
	 * @param dateTime
	 * @return int as Value object
	 */
	public static Value year(DateTime dateTime) {
		try {
			if (null != dateTime) {
				logger.debug(FORMAT_CONSTANT, DATE_FUNCTIONS_YEAR, dateTime.getYear());
				return new Value(BigDecimal.valueOf(dateTime.getYear()));
			}
		} catch (Exception e) {
			logger.error(FORMAT_CONSTANT, DATE_FUNCTIONS_YEAR_EXCEPTION, e.getMessage());
		}
		return null;
	}
	
	/**
	 * 
	 * @param dateTime
	 * @return int as Value object
	 */
	public static Value month(DateTime dateTime) {
		try {
			if (null != dateTime) {
				logger.debug(FORMAT_CONSTANT, DATE_FUNCTIONS_YEAR, dateTime.getMonthOfYear());
				return new Value(BigDecimal.valueOf(dateTime.getMonthOfYear()));
			}
		} catch (Exception e) {
			logger.error(FORMAT_CONSTANT, DATE_FUNCTIONS_YEAR_EXCEPTION, e.getMessage());
		}
		return null;
	}
	
	/**
	 * 
	 * @param dateTime
	 * @return int as Value object
	 */
	public static Value day(DateTime dateTime) {
		try {
			if (null != dateTime) {
				logger.debug(FORMAT_CONSTANT, DATE_FUNCTIONS_YEAR, dateTime.getDayOfMonth());
				return new Value(BigDecimal.valueOf(dateTime.getDayOfMonth()));
			}
		} catch (Exception e) {
			logger.error(FORMAT_CONSTANT, DATE_FUNCTIONS_YEAR_EXCEPTION, e.getMessage());
		}
		return null;
	}

	/**
	 * 
	 * @param dateTime
	 * @param numberOfMonths
	 * @return dateTime
	 */

	public static Value addMonths(DateTime dateTime, Integer numberOfMonths) {
		try {
			if (null != dateTime) {
				return new Value(dateTime.plusMonths(numberOfMonths));
			}
		} catch (Exception e) {
			logger.error(" << DateFunctions : perform() : addMonths : Exception : {}", e.getMessage());
		}
		return null;
	}
	
	
	/**
	 * 
	 * @param dateTime1 , dateTime2 
	 * @return int as Value object
	 */
	public static Value dateDiff(DateTime dateTime1, DateTime dateTime2) {
		logger.debug(" << DateFunctions : perform() : dateDiff : Input  : date1 - {},  date2 - {}", dateTime1,
				dateTime2);
		long dateDiff = dateTime1.getMillis() - dateTime2.getMillis();
		logger.debug(" << DateFunctions : perform() : dateDiff : Result : {}", dateDiff);
		logger.info(" << DateFunctions : perform() : dateDiff : Result : {}", dateDiff/(3600 * 1000 * 24));
		return new Value(BigDecimal.valueOf((dateDiff/(3600 *  1000 * 24))));
	}
	
	/**
	 * 
	 * @return System date with TimeZone.
	 */
	
	public static Value currentSystemDateTime() {
		logger.debug("{}"," << DateFunctions : perform() : currentSystemDate ");
		return new Value(new DateTime());
	}

	public static Value now() {
		return new Value(DateTime.now(DateTimeZone.UTC));
 	}
}
