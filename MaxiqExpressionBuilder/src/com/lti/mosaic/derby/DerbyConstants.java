package com.lti.mosaic.derby;

/**
 * @author rushikesh
 *
 */
public class DerbyConstants {

	private DerbyConstants() {
	}

	public static final String PROTOCOL = "jdbc:derby:memory:";
	public static final String DRIVER = "org.apache.derby.jdbc.EmbeddedDriver";

}
